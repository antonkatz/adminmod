import pprint
import string
import re


class LOG:
    log_file = 'log.html'
    css = '''CSS'''
    html = ''''''
    repeating_block = '''<div class="block" style="background-color:#D8D8D8;margin-right:5px;display:inline-block;width:{3}px;position:absolute;left:{2}px"><div style="font-weight:600">{0}</div>{1}</div>'''

    width = 400
    position = 0

    highlight = "'id': [0-9]+"
    highlight_wrap = '<span style="font-weight:600;color:blue">{0}</span>'

    pp = pprint.PrettyPrinter(indent=4)

    buffer_block = ''

    def __init__(self):
        self.log_write = open(self.log_file, 'w+')
        #self.log_write.write(self.html)
        self.log_write.close()

    def logStore(self, data, title=None):
        data = self.pp.pformat(data)
        data = re.sub(self.highlight, lambda match: self.highlight_wrap.format(match.group(0)), data)
        self.buffer_block += '<br>' + data
        if title:
            self.log(title)

    def log(self, title):
        log_write = open(self.log_file, 'a')
        repeating_block = self.repeating_block.format(title, self.buffer_block, (self.position*self.width), self.width)
        self.buffer_block = ''
        repeating_block = string.replace(repeating_block, '\n', '<br>')
        log_write.write(repeating_block)
        log_write.close()

        self.position += 1
