RegionManager = (function (Backbone, $) {
    var currentView;
    var el = "page";
    var region = {};
    var view_base;

    var self = this;
    this.active = false;

    var closeView = function () {
        if (window.current_view && window.current_view.close && window.current_view_el == el) {
            window.current_view.close();
        }
    };
 
    var openView = function (view) {
        window.current_view = new view_base;
        window.current_view_el = el;
        window.current_view.render(view, active);
        
        $("#"+el).html(window.current_view.el);
        if (window.current_view.onShow) {
            window.current_view.onShow(active);
        }
    };
 
    region.show = function (base, view, element, active) {
        self.active = active;
        el = element;
        view_base = base;
        closeView();
        openView(view);
    };
 
    return region;
})(Backbone, jQuery);

 
(function($){  
    window.bbView = Backbone.View.extend({
      render: function(view, active){
        var template = $("#"+view+"_t").html();
          console.log(view);
        if (!active){
            $(this.el).hide();
        }
        $(this.el).html(_.template(template, {}));
      },
     
      close: function(){
        this.remove();
        this.unbind();
      },
     
      onShow: function(active){
        $(this.el).show();
        var delay = 500;
        if (active){
            delay = 0;
        }else{
            $(this.el).offset({left: ($(this.el).width() * -1)});
            $(this.el).animate({left:'0px'}, delay);
        }
      }
    });
    
    //RegionManager.show(window.formView, 'new_order');
    //setTimeout(function(){window.RegionManager.show(window.formView, 'test');}, 4000);
    
    //setTimeout(function(){RegionManager.show(window.formView, 'new_order');}, 5000);

})(jQuery);
