(function($){    
    window.page_socket.on('switch_view', function(data, active){
        //console.log(active);
        RegionManager.show(window.bbView, data['id'], data['html_el'], active);
        //window.ko.cleanNode($("#"+data['html_el'])[0]);
        controlHooks();
        console.log(data, active);
        });  
    
    window.form_socket.on('log', function(data){
        $('#log').append('<div>'+ data +'</div>');
        });
    window.form_socket.on('error', function(data){
        $('#error').append('<div>'+ data +'</div>');
        });
    window.form_socket.on('fields', function(_data, html_el){
        console.log(_data, 'FORM', window.active_ko_models);

        var model = new window.FormViewModel();
        model.pushFields(_data);
        window.active_ko_models[html_el] = model;
        window.ko.applyBindings(window.active_ko_models[html_el], document.getElementById(html_el));
        window.styleJS();
        });
//    window.form_socket.on('update', function(data, html_el){
//        window.ko.cleanNode($("#"+html_el)[0]);
//        var model = window.active_ko_models[html_el];
//        model.pushFields(data);
//        window.ko.applyBindings(model, document.getElementById(html_el));
//    });
    
    control_socket.on('log', function(data){
        $('#log').append('<div>'+ data +'</div>');
    });
    control_socket.on('controls', function(data, html_el){
        console.log(data, 'CONTROL');
        //window.control_model = new window.ControlViewModel(data);
        window.ko.applyBindings(new window.ControlViewModel(data), document.getElementById(html_el));
        window.styleJS();
    });

    //SINGLE CONTROLS
    //Make a large single control function as to prevent sending data before it is generated/spliced
    //window.control_socket.emit('controlRequest', control['id'], batches.splice(z, 1));

})(jQuery);