WEB_SOCKET_SWF_LOCATION = '/WebSocketMain.swf';
window.page_socket = io.connect(':5000/page');
window.form_socket = io.connect(':5000/form');
window.control_socket = io.connect(':5000/control');
window.init_socket = io.connect(':5000/init');

window.active_ko_models = {};

window.FormViewModel = function () {
    //this.field_data = ko.observableArray(fields);
    var self = this;
    this.visible = true;

    this.pushFields = function(fields){
        var batches = [];
        if (fields.length == 0){
            self.visible = false;
        }
        //console.log(fields);
        $.each(fields, function(z, batch){

            batches.push({'field_data': ko.observableArray(batch),
                        'sControl': function(data, event){window.batch = batches[z]; window.sControl(event);},
                        'uploadFile': function(data, event){window.batch = batches[z];window.uploadFile(event);}});

            $.each(batches[z]['field_data'](), function(i, field){
                var subscribe_to;
                var subscribe_part;
                if (typeof field['data_request']['selected'] != 'undefined'){
                    field['data_request']['selected'] = ko.observable(field['data_request']['selected']);
                    subscribe_to = field['data_request']['selected'];
                    subscribe_part = 'selected'
                }else{
                    field['data_request'] = ko.observable(field['data_request']);
                    subscribe_to = field['data_request'];
                    subscribe_part = '';
                }
                var subscribeFunc = function(data_emit){
                    console.log('update', subscribe_part);
                    var field_local = {};
                    jQuery.extend(field_local, field);
                    if (subscribe_part){
                        //console.log('exec');
                        //subscribe_to.subscribe(function(r){alert('h')});
                        field_local['data_request'] = {};
                        field_local['data_request'][subscribe_part] = data_emit;
                    }else{
                        field_local['data_request'] = data_emit;
                    }
                    window.form_socket.emit('update', field_local);
                }

                subscribe_to.subscribe(subscribeFunc);
                subscribe_to.extend({ notify: 'always' });

                field['name'] = field['name'].capitalize() + ': ';
            });
        });

        self.batches = batches;
    }
}

window.ControlViewModel = function (controls) {
    this.control_data = ko.observableArray(controls);
    
    controlRequest = function(control){
        //console.log(control);
        window.control_socket.emit('controlRequest', control['id']);
    };
    
    $.each(this.control_data(), function(i, control){
        control['name'] = control['name'].capitalize();
    });
}