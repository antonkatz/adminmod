window.styleJS = function(){
    window.bmd = function(elem){
        $(elem.target).addClass('button_down');
    }
    window.bmu = function(elem){
        $(elem.target).removeClass('button_down');
    }

    $(".button").each(function() {
        $(this).mousedown(window.bmd);
        $(this).mouseup(window.bmu);
    });
    $(".delete.button").each(function() {
        $(this).click(function(elem){$(elem.target).parent().hide();});
    });
    $(".add").each(function(){
        $(this).click(function(elem){
            $(".selector_div").html('');
        });
    });
    $(".selector").each(function(){
        $(this).click(function(elem){
           $('.selector_div').html('');
            //console.log($('.selector_wrap'));
           //$('#client_edit').show();
           $('#add_record').hide();
        });
    });
    $('.search').keyup(function(){$('.selector_wrap').show();$('#client_edit').html('');});
}

window.sControl = function(event, empty){
    if (!empty){
        var data = window.batch.field_data();
    }
    console.log('empty', empty);
    var id = event.target.id;
    var elem = event.target;
    if (!id){
        while (!id){
            id = elem.parentElement.id;
            elem = elem.parentElement;
        }
    }
    console.log('control id', id, event);
    window.control_socket.emit('controlRequest', id, data);
    console.log('sControl', data);
}

window.uploadFile = function(event){
    console.log('uploadFile', event);
    var packet = window.batch.field_data();

    var files = event.target.files;
    var file = files[0];

    var reader = new FileReader();
    reader.onloadend = function(){
        window.form_socket.emit('upload', reader.result, packet);
        //$('#testimg').attr('src', reader.result);
        //console.log(reader.result);
    };

    reader.readAsDataURL(file);
    //Says encoding option for text is not yet supported
    //reader.readAsBinaryString(file);
}

var controlHooks = function(){
    console.log($(".form_submit"));
    $(".form_submit").unbind();
    $(".control").unbind();
    //$(".upload").unbind();
    //console.log('----', $._data(document.getElementById('form_submit'), "events"));
    $('.form_submit.rT').click(function(obj){window.form_socket.emit('submit', true); console.log('SUBMIT RT', obj)});
    $('.form_submit.rF').click(function(obj){window.form_socket.emit('submit'); console.log('SUBMIT RF', obj)});
    //console.log('----\t', $._data(document.getElementById('form_submit'), "events"));

    $(".control").click(function(event){window.sControl(event, true)});
    //$(".upload").bind('change', function(event){window.uploadFile(event)});

    $('#add_record').click(function(event){$(event.target).hide();});

//    if (!window.File && !window.FileReader && !window.FileList && !window.Blob) {
//        alert('The File APIs are not fully supported in this browser.');
//    }
}