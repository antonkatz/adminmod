Notes on initial connections:
    The frontend can connect to the backend in any order. However, there is a lock preventing from anything executing before the initCon handler has been ran. This is achieved
    through gevent Event. The initial views are set after initCon is done.

Misc notes:
    converter.filldata:
    Manges receiving data.
    In general: IN - at load time. OUT - after load.

    Form.on_load:
    In respect to sending data - check whether a local storage of the data is available (in memory session object). If not uses the filldata (in) to fill it out.

    For instances when a form must repeat. (ex. settings). Unknown number of repeats before load. Every repeat gets a batch number attached. The batch number must also be attached
    to the field.

    Currently all data from forms is stored in memory on change of said data. The requirement check runs against this model.
    'data_request' and 'data_store' have become part of the inflexible core of the system

    For further extendability allow for replacement of main classes with files located anywhere in the system.

    data_request serves as a pointer to where to place the data request and also as storage for the retrieved data

    pg_storage is initiated within initCon

    !! filldata must be split up into 3 classes and renamed. Some functions repeat. Fine for now.