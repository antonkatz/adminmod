import gevent.monkey; gevent.monkey.patch_all()
import psycopg2, psycopg2.extras
from .. import settings, config
from _logging import LOG
import sys
from string import Formatter
import time

class storage:
    #def __init__(self):
        #self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
        #self.formatter = Formatter
        #self.logger = LOG()

    def cursorReload(self):
        try:
            self.cursor.close()
            self.con.close()
            self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
        except:
            self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
        try:
            self.cursor = self.con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        except BaseException as e:
            print e

    def setDispatch(self, set_id, o_data=[]):
        self.con = psycopg2.connect(host=settings.pg_host, password=settings.pg_pass, user=settings.pg_user, database=settings.pg_db)
        self.cursor = self.con.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        result_set = []
        #formatter = Formatter()
        set = filter(lambda set: set['id'] == set_id, config.query_set_list)[0]
        query_list = set['set']

        data = o_data
        for query_id in query_list:
            query_obj = filter(lambda query: query['id'] == query_id, config.query_list)[0]
            func = getattr(self, query_obj['function'])

            temp_data = []
            rm = []
            if len(data) == 0:
                data = [None]
            for _data in data:
                result = func(query_obj, _data)
                print 'pg', query_id, _data
                print result
                if not result:
#                   if len(result) == 0:
                    rm.append(_data)
                else:
                    for formatted_result in self.getResult(None, result, query_id):
                        copy = formatted_result.copy()
                        #copy.update(o_data)
                        #if not 'save_original' in query_obj:
                        if _data:
                            copy.update(_data)
                        ###Not complete save original

                        temp_data.append(copy)
                        #print 'lq:', result
                        result_set.append({'id': query_id, 'result': result})
                if 'repeat' in query_obj:
                    if not query_obj['repeat']:
                        break
            data = temp_data

        #for r in rm: data.remove(r)

        self.cursor.close()
        self.con.close()

        #print 'pg_dispatch:', data
        if 'final_format' in set:
            #result = self.getResult(set['final_format'], result_set)
            result = []
            for row in data:
                if row:
                    subset = dict()
                    for pointer in set['final_format']:
                        if pointer in row:
                            subset[pointer] = row[pointer]
                    result.append(subset)
            print 'pg_dispatch final:', str(result)[0:1000]
            return result
        #self.cursorReload()

    def getResult(self, pointers, result_set, query_id=None):  # Can parse a single result or a result_set of multiple
        ###Repeating block
        def iterator(data, id):
            for row in data:
                subset = dict()
                for key, value in row.iteritems():
                    pointer = str(id) + '.' + str(key)
                    if pointers:
                        if pointer in pointers:
                            subset[pointer] = value
                    else:
                        #if not pointer in subset:
                        #    subset[pointer] = []
                        subset[pointer] = value
                return_list.append(subset)
        ###
        return_list = []
        if not query_id:
            for result in result_set:
                data = result['result']
                id = result['id']
                iterator(data, id)
        else:
            iterator(result_set, query_id)

        return return_list
        # else:
        #     return_dict = dict()
        #     for key, value in result_set.iteritems():
        #         pointer = str(query_id) + '.' + str(key)
        #         return_dict[pointer] = value
        #     return return_dict

    def queryLoad(self, query_obj, data):
        query = query_obj['query']

        #try:
        print 'queryLoad', query, data
        list_fetched = 0
        keys = []
        result = []
        if data:
            for key, value in data.iteritems():
                if type(value) is list:
                    #if type(value[0]) is list:
                    if len(value) > list_fetched:
                        list_fetched = len(value)
                    keys.append(key)

        print('\n')
        #This assumes that all lists are the same length
        if list_fetched:
            print('list_fetched', list_fetched)
            temp_result = []
            flag_break = False
            self.list_fetched_len = range(list_fetched)
            for i in self.list_fetched_len:
                modified_data = data.copy()
                print(self.list_fetched_len)
                print(modified_data, keys)

                # This is what modified_data does:
                # Takes the data in it's original, and if there is list, takes out the corresponding value
                # Number of times (and presence of list) is determined by list_fetched, which fetches the largest list in the data dictionary and returns its length
                # Sometimes lists are different lengths
                for key in keys:
                    if isinstance(modified_data[key], list):
                        try:
                            modified_data[key] = modified_data[key][i]
                        except:
                            #self.list_fetched_len = range(len(modified_data[key]))  # Subbing range for list_fetched. item is not used anyways.
                            #modified_data[key] = modified_data[key][::][0]
                            if 'follow_key' in query_obj:
                                if key == query_obj['follow_key']:
                                    flag_break = True
                if flag_break:
                    break

                _query = query.format(modified_data)
                print('_query:', _query)
                self.cursor.execute(_query)
                temp_result += [_item for _item in self.cursor.fetchall()]
            result = []
            if len(temp_result) > 0:
                result.append({})
                for key in temp_result[0]:
                    result[0][key] = [dictionary[key] for dictionary in temp_result]

        else:
            #In case there is no data to use, or some missing just skip.
            try:
                query = query.format(data)
            except TypeError:
                return []
            self.cursor.execute(query)

            if 'format_multiple' in query_obj:
                result = []
                temp_result = [_item for _item in self.cursor.fetchall()]
                if len(temp_result) > 0:
                    result.append({})
                    for key in temp_result[0]:
                        result[0][key] = [dictionary[key] for dictionary in temp_result]
            else:
                result = self.cursor.fetchall()
        print "\t\t", result
        # except BaseException as e:
        #     print 'error queryLoad ' + str(e)
        #     return []
        # #print query

        return result

    #This one checks in terms of count.
    def queryStore(self, query_obj, data):
        check = query_obj['check']
        check = check.format(data)

        self.cursor.execute(check)
        count = self.cursor.fetchone()['count']
        if count >= len(query_obj['query']):
            count = len(query_obj['query']) - 1

        query = query_obj['query'][count]
        if not query:
            return
        print 'queryStore', data, query_obj
        query = query.format(data)

        self.cursor.execute(query)
        self.con.commit()

    def queryStoreBoolPointer(self, query_obj, data):  # For when you need to store something and you either have a pointer or it's false
        pointer = query_obj['pointer']
        query_num = 0
        if pointer in data:
            if data[pointer]:
                query_num = 1

        #Putting NULL instead of an empty value
        for key, value in data.iteritems():
            if value == '':
                data[key] = 'NULL'

        query = query_obj['query'][query_num]
        if query:
            print query, data
            query = query.format(data)
            print "queryStoreBool", query, data
            #sys.exit()
            #self.cursor.mogrify
            self.cursor.execute(query)
            self.con.commit()

            #Sometimes there is no returning statement in SQL
            try:
                pointer_fresh = self.cursor.fetchone()
                return [pointer_fresh]  # The list is for proper processing at setDispatch result formatting
            except:
                pass
            #Never, never, NEVER use Python string concatenation (+) or string parameters interpolation (%) to pass variables to a SQL query string. Not even at gunpoint.

    def queryStoreBoolPointerNI(self, query_obj, data):  # For when you need to store something and you either have a pointer or it's false
        pointer = query_obj['pointer']
        query_num = 0
        if pointer in data:
            if data[pointer]:
                query_num = 1

        for key, value in data.iteritems():
            if not value and value != False and not isinstance(value, int):
                data[key] = 'NULL'

        query = query_obj['query'][query_num]
        if query:
            #query = query.format(data)
            new_data = {}
            for key, value in data.iteritems():
                new_data[str(key)] = value
            data = new_data

            print "queryStoreBoolNI", query
            #sys.exit()
            #self.cursor.mogrify
            self.cursor.execute(query, data)
            self.con.commit()

            #Sometimes there is no returning statement in SQL
            try:
                pointer_fresh = self.cursor.fetchone()
                return [pointer_fresh]  # The list is for proper processing at setDispatch result formatting
            except:
                pass
            #Never, never, NEVER use Python string concatenation (+) or string parameters interpolation (%) to pass variables to a SQL query string. Not even at gunpoint.