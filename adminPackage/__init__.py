import gevent; gevent.coros; gevent.monkey.patch_all()

from socketio import socketio_manage
from handler.form import Form
from handler.control import Control
from handler.misc import Page, initCon


class Application(object):
    def __call__(self, environ, start_response):
        request = {'gevent_event': gevent.event.AsyncResult(),
                   'gevent_group': gevent.pool.Group(),
                   'box': None,
                   'box_lock': gevent.coros.Semaphore()}
        server = socketio_manage(environ, {'/form': Form, '/control': Control, '/page': Page, '/init': initCon}, request)