import gevent.monkey; gevent.monkey.patch_all()
from .. import config


class FormControl():
    def __init__(self, session):
        self.session = session
        self.form = session['endpoints']['form']

    def deleteBatch(self, args, packet):
        additional = []
        # A hackish way to delete additional fields coming from present args
        for id in args:
            _sub = dict(packet[0])
            _sub['id'] = id
            additional.append(_sub)
        packet += additional
        for p_field in packet:
            print 'deleteBatch', p_field
            dt_field = filter(lambda field: field['id'] == p_field['id'] and field['batch_id'] == p_field['batch_id'], self.session['data_store'])[0]
            self.session['data_store'].remove(dt_field)

            if (p_field['id'], p_field['batch_id']) in self.form.save_lock:
                self.form.save_lock.remove((packet['id'], packet['batch_id']))
            if (p_field['id'], p_field['batch_id']) in self.form.save_lock_fatal:
                self.form.save_lock_fatal.remove((packet['id'], packet['batch_id']))

    def selectBatch(self, args, packet):
        batch_id = packet[0]['batch_id']
        view_id = self.form.view_config['id']
        fields = filter(lambda field: view_id in field['view'], config.field_list)
        field_ids = [field['id'] for field in fields]

        #print view_id, field_ids, batch_id
        print 'selectB ds:', self.session['data_store']
        remove = []
        for dt in self.session['data_store']:
            if dt['id'] in field_ids and dt['batch_id'] != batch_id:
                remove.append(dt)
        for rm in remove:
            self.session['data_store'].remove(rm)
        print 'remove', batch_id, field_ids, view_id
        if args:
            self.skipReload(args)

    # For fields that always get reloaded
    def skipReload(self, field_ids):
        self.form.skip_reload = field_ids

    #There could only be one copy of each field in data_store
    def collapseDataStore(self, args, packet):
        f_from = args['from']
        f_to = args['to']
        #f_format = args['format']

        ##print f_from, 'f_from'
        print 'collapse:', self.session['data_store']
        for i, f_field_id in enumerate(f_from):
            print f_field_id
            field_data = filter(lambda field: field['id'] == f_field_id, self.session['data_store'])[0]
            merge_field = filter(lambda field: field['id'] == f_to[i], self.session['data_store'])
            if len(merge_field) > 0:
                batch_id = max(merge_field, key=lambda field: field['batch_id'])['batch_id'] + 1
            else:
                batch_id = 0
            field_data['batch_id'] = batch_id
            field_data['id'] = f_to[i]
            print 'COLLAPSE', field_data
            #merge_field['data_request'] = field_data
            #merge_field['to_db'] = field_data

        # for field_id in f_format:
        #     field = filter(lambda field: field['id'] == field_id, self.session['data_store'])[0]
        #     data = field['data_request']
        #     field['to_db'] = data

    def removeFields(self, args, *_args):
        self.session['filldata']['out'].removeFields(None, args)

    def clearLock(self, args):
        remove = [field[0] for field in self.form.save_lock+self.form.save_lock_fatal]
        self.session['filldata']['out'].removeFields(None, {'fields': remove})
        self.form.save_lock = []
        self.form.save_lock_fatal = []

    def sendEmail(self, args, packet):
        batch = packet[0]['batch_id']
        content = filter(lambda ds: ds['id'] == args['content'] and ds['batch_id'] == batch, self.session['data_store'])[0]['data_request']
        to = args['to']

        print batch, to, content

    def updateDB(self, args, packet):
        print 'updateDB', packet

        pg = self.session['pg_storage']
        filldata = self.session['filldata']['out']

        data = filldata.datastoreToQuery(fields=args['fields'], batch_id=packet[0]['batch_id'])
        pg.setDispatch(args['query_set_id'], data)

    def default(self, args, packet):
        self.session['filldata']['in'].default(args)
