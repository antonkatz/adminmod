from gevent import monkey; monkey.patch_all()

from .. import config


class PageControl:
    def __init__(self, session_endpoints):
        self.session_endpoints = session_endpoints
        self.emit = session_endpoints['page'].emit
        self.active_views = {}
        self.view_history = []
        self.prev_view = None
        self.pinned_view = {}
        
    def switchView(self, view_id, *args):
        self.view = filter(lambda view: view['id'] == view_id, config.view_list)[0]
        #attrs_to_send = ['id', 'html_el']
        view = {'id': self.view['id'], 'html_el': self.view['html_el']}

        self.viewHistory(view_id)

        active = False
        if view_id == self.prev_view:
            active = True
        self.prev_view = view_id
        self.emit('switch_view', view, active)

        if self.view['type']:
            endpoint = self.session_endpoints[self.view['type']]
            endpoint.current_html_el = self.view['html_el']
            endpoint.on_load(view_id)

    def pinView(self, control_info, *args):
        #Adds view to a list that control.py checks before assigning a function to a frontend call
        #Upon the call control deletes it from the pinned list
        self.pinned_view.update(control_info)

    #Should be deprecated. At least for now. Absolutely no need for it.
    #def manageActiveViews(self, view):
        #if view in config.
        #check_html_el = filter(lambda active_view: view['html_el'] == active_view['html_el'], self.active_views)
        #html_el = view['html_el']
        #view_id = view['id']


        #if html_el in self.active_views:
            #if self.active_views[html_el] == view_id:
            #else:
            #    self.active_views[html_el] = view_id
        #    pass
        #else:
        #    self.active_views[html_el] = view_idS

    def viewHistory(self, view_id):
        self.view_history.append(view_id)
        if len(self.view_history) > 20:
            del self.view_history[0]