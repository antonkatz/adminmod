from gevent import monkey; monkey.patch_all()

import smtplib
from email.mime.multipart import MIMEMultipart, MIMEBase
from email.mime.text import MIMEText

from .. import settings


class CommsControl():
    def __init__(self, session):
        self.session = session

    #to, subject, text, attach
    def email(self, args, packet):
        #batch_id = packet[0]['batch_id']
        data_store = self.session['data_store']

        to = filter(lambda field: field['id'] == args['to'], data_store)
        subject = filter(lambda field: field['id'] == args['subject'], data_store)[0]['data_request']
        text = filter(lambda field: field['id'] == args['content'], data_store)[0]['data_request']
        print 'email to:', to

        for recipient in to:
            recipient = recipient['data_request']
            msg = MIMEMultipart()
            msg['From'] = settings.gmail_user
            msg['To'] = recipient
            msg['Subject'] = subject

            msg.attach(MIMEText(text))

            #part = MIMEBase('application', 'octet-stream')
            #part.set_payload(open(attach, 'rb').read())
            #Encoders.encode_base64(part)
            #part.add_header('Content-Disposition',
            #        'attachment; filename="%s"' % os.path.basename(attach))
            #msg.attach(part)

            mailServer = smtplib.SMTP("smtp.gmail.com", 587)
            mailServer.ehlo()
            mailServer.starttls()
            mailServer.ehlo()
            mailServer.login(settings.gmail_user, settings.gmail_pass)
            mailServer.sendmail(settings.gmail_user, recipient, msg.as_string())
            # Should be mailServer.quit(), but that crashes...
            mailServer.close()