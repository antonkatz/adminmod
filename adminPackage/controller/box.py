#This class has to be written to be initiated on every connection
#This way it will allow for an easy user initiated restart

#For now the control will look for a folder with the right name, but later on it will have to create it and rename it
from gevent import monkey; monkey.patch_all()
from PIL import Image, ImageFile
from StringIO import StringIO
from .. import config, settings
import requests
import pickle, cPickle
import psycopg2.extras
import json
import pprint
from datetime import timedelta, datetime
import logging

class BoxControl:
    def __init__(self, session, requests):
        #Setting up
        self.requests = requests
        self.session = session
        self.pg = session['pg_storage']
        self.queries = config.box_queries
        self.client_id = settings.box_data['client_id']
        self.client_secret = settings.box_data['client_secret']
        self.retoucher_folder_id = settings.box_data['retoucher_folder_id']

        self.box_data = requests['box']
        self.auth_header_template = 'Bearer {0}'
        self.auth_header = {'Authorization': None}

        self.next_refresh = datetime.now()
        self.glock = requests['box_lock']
        #First step is to get the token
        #Check memory (requests), then db
        if not self.box_data:
            try:
                self.pg.cursor.execute(self.queries['retrieve'])
            except:
                self.pg.cursorReload()
                self.pg.cursor.execute(self.queries['retrieve'])

            result = self.pg.cursor.fetchone()
            self.box_data = cPickle.loads(result['pickle'])
            #self.box_data = {'refresh_token': 'GdkXNPRODodz9AemI6jvWtnYo4uM1DyqqX5Vahiv4fxBhgJ7t299geU134jPc731'}
            self.requests['box'] = self.box_data

        self.refreshToken()
        #folder_id = self.execRequest(self.findFolder, {'folder_name': '5420fallingbrook48', 'parent': settings.box_data['retoucher_folder_id']})['folder_id']

    def downloadFolder(self, folder_id):
        #Downloads all files in a folder, and then yields {contents, file_id, file_name} one by one
        url = 'https://api.box.com/2.0/folders/{0}/items'.format(folder_id)
        files = requests.get(url, headers=self.auth_header)
        for file in files.json()['entries']:
            if file['type'] == 'file':
                url = 'https://api.box.com/2.0/files/{0}/content'.format(file['id'])
                try:
                    content = requests.get(url, headers=self.auth_header).content
                    yield {'content': content, 'file_id': file['id'], 'name': file['name']}
                except:
                    self.session['endpoints']['form'].emit('error', 'File could not be downloaded. Please restart process')

    def reduceSize(self, image_file_obj, ext=None):
        #Taken from the original box app. Modified to work only with JPEGs (ext set to None).
        image_store = StringIO(image_file_obj)
        image_save = StringIO()
        image = Image.open(image_store)

        try:
            #image.save(image_save, self.config.pil_format[ext], quality=self.settings.blog_image_quality, optimize=True)
            image.save(image_save, 'JPEG', quality=settings.blog_image_quality, optimize=True)
        except IOError:
            ImageFile.MAXBLOCK = image.size[0] * image.size[1]
            image.save(image_save, 'JPEG', quality=settings.blog_image_quality, optimize=True)
            #image.save(image_save, self.config.pil_format[ext], quality=self.settings.blog_image_quality, optimize=True)

        jpeg = image_save.getvalue()
        image_store.close()
        image_save.close()

        return jpeg

    def findFolder(self, folder_name, parent=None):
        #Returns the request obj if no parent specified. Otherwise returns dict {status_code, folder_id}
        #Searches for a folder that has parent in path_collection. Returns first found.
        url = 'https://api.box.com/2.0/search'
        params = {'query': folder_name}
        request = requests.get(url, params=params, headers=self.auth_header)
        if parent:
            data = request.json()['entries']
            for entry in data:
                path = entry['path_collection']['entries']
                for folder in path:
                    if int(folder['id']) == parent:
                        return {'status_code': request.status_code, 'folder_id': entry['id']}
            return False
        else:
            return request

    def execRequest(self, func, kwargs={}):
        #For executing requests. On 401 will refresh token and try again.
        #func must return request obj, or status_code (possible in a dict)
        try:
            r = func(**kwargs)
            if isinstance(r, dict):
                status_code = r['status_code']
            elif isinstance(r, int):
                status_code = r
            elif not r:
                return False
            else:
                status_code = r.status_code

            if status_code == 401:
                self.refreshToken()
                r = func(**kwargs)
            return r

        except BaseException as e:
            self.refreshToken()
            try:
                r = func(**kwargs)
                return r
            except BaseException as e:
                print('Could not execute request', e)
                raise

    def refreshToken(self, response=None):
        self.glock.acquire()
        #Checks response status. If invalid_token refreshes. If no response given refreshes.
        #Then saves to DB
        flag = True
        if response:
            if not response.status_code == 401:
                flag = False

        now = datetime.now()
        if now < self.next_refresh:
            logging.info('Box token has not expired yet')
            self.glock.release()
            return
        self.next_refresh = now + timedelta(minutes=50)

        if flag:
            params = {'grant_type': 'refresh_token', 'refresh_token': self.requests['box']['refresh_token'], 'client_id': self.client_id, 'client_secret': self.client_secret}
            refresh = requests.post('https://www.box.com/api/oauth2/token', data=params)
            if refresh.status_code == 200:
                self.box_data = refresh.json()
                self.requests['box'] = self.box_data
                print self.box_data
                _pickle = psycopg2.extensions.adapt(cPickle.dumps(self.box_data))
                query = self.queries['save'].format(_pickle)
                try:
                    self.pg.cursor.execute(query)
                    self.pg.con.commit()
                except BaseException as e:
                    try:
                        self.pg.cursorReload()
                        self.pg.cursor.execute(query)
                        self.pg.con.commit()
                    except BaseException as e2:
                        print('Could not save refreshed box token', e, e2)
                #Updating authorization header to use in requests
                self.auth_header['Authorization'] = self.auth_header_template.format(self.box_data['access_token'])
            else:
                self.box_data = self.requests['box']
                print('Could not refresh box token')
        self.glock.release()

    def listMusic(self, folder_id):
        #Yields music links that have both types of files present. (name, [links], [types])
        types = ['ogv', 'ogg', 'mp3']
        _temp = {}

        try:
            url = 'https://api.box.com/2.0/folders/{0}/items'.format(folder_id)
            self.refreshToken()
            files = requests.get(url, headers=self.auth_header)
            for file in files.json()['entries']:
                if file['type'] == 'file':
                    url = 'https://api.box.com/2.0/files/{0}'.format(file['id'])
                    try:
                        link = requests.put(url, headers=self.auth_header, data='{"shared_link": {"access": "open"}}').json()
                        name, type = link['name'].split('.')[0], link['name'].split('.')[1]
                        if not name in _temp:
                            _temp[name] = {}
                            _temp[name]['types'] = []
                            _temp[name]['links'] = []

                        _temp[name]['types'].append(type)
                        _temp[name]['links'].append(link['shared_link']['download_url'])
                    except:
                        self.session['endpoints']['form'].emit('error', 'Could not get music files')
        except BaseException as e:
            self.session['endpoints']['form'].emit('error', 'Listing music failed: '+str(e))

        # Checking if all types are present
        for file_name, value in _temp.iteritems():
            _types = list(value['types'])
            for i, type in enumerate(value['types'][::-1]):
                if type in types:
                    _types.remove(type)
            if not _types:
                yield (file_name, value['links'], value['types'])