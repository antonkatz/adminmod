from gevent import monkey;

monkey.patch_all()
import gevent

from socketio.namespace import BaseNamespace
from json import dumps
from ..converter import basic, filldata
from ..check.form import formCheck
from .. import config
import datetime
import logging
import copy

from _logging import LOG


class Form(BaseNamespace):
    def recv_connect(self):
        self.request['gevent_group'].add(gevent.spawn(self.init))

    def init(self):
        self.request['gevent_event'].get()
        self.session['endpoints']['form'] = self

        self.session['load_store'] = []
        self.session['data_store'] = []

        self.session['filldata'] = {'in': filldata.fillDataIn(config.field_list, self.session),
                                    # Can be replaced with absolute path to a file in the settings. should include later.
                                    'out': filldata.fillDataOut(config.field_list, self.session)}

        self.form_check = formCheck(self, self.session)  # Contains reqCheck to check fields for requirements on_update

        self.save_lock = []
        self.save_lock_fatal = []
        self.pending_unlock = {}  # Several forms will cause problems. Must be cleared at some time.

        self.view_store = {}
        self.skip_reload = []
        self.exec_child = True
        self.skip_children = []

        self.child_break = False
        self.batch_id = 0  # Need it after all. Even with crazy high numbers it should still be fine.

        self.submit_refresh = False  # Needed so that when a form goes through submitting of children it keeps in mind that it needs to refresh

    def on_load(self, view_id):
        self.view_config = filter(lambda view: view['id'] == view_id, config.view_list)[0]
        self.fields = filter(lambda field: view_id in field['view'], config.field_list)
        data_store = self.session['data_store']
        fields_reload = [field for field in config.fields_reload if not field in self.skip_reload]

        if 'child' in self.view_config:
            self.skip_children = []

        #This function only works for fetching pointers
        #Prefetches from load_store before main fetching begins
        if 'prefetch' in self.view_config:
            for pointer, field_id in self.view_config['prefetch'].iteritems():
                field = filter(lambda ls: ls['id'] == pointer, self.session['load_store'])
                if field:
                    field = field[0]
                    field['id'] = field_id
                    field['batch_id'] = 0  # Is this necessary?
                    field['data_request'] = field['data_request'][0]
                    self.session['data_store'].append(field)

        ###print view_id, self.session['data_store']
        #This part is needed to avoid creating duplicate extra fields
        del_batch = []
        #This filter is needed so that fields don't get deleted by other forms loading
        view_field_ids = [f['id'] for f in self.fields]
        view_data_store = filter(lambda field: field['id'] in view_field_ids, data_store)
        for field in view_data_store:
            #extra_batches = filter(lambda data_store: data_store['batch_id'] == batch_id, data_store)
            #flag = False
            if not field['data_request']:
                if 'extra' in field:
                    if field['extra']:
                        del_batch.append(field['batch_id'])
            else:
                if 'extra' in field:
                    field['extra'] = False
                    if field['batch_id'] in del_batch:
                        del_batch.remove(field['batch_id'])
        for del_id in del_batch:
            fields = filter(lambda data_store: data_store['batch_id'] == del_id, data_store)
            print 'fields deleted:', fields
            for field in fields:
                data_store.remove(field)

        attrs_to_send = ['id', 'data_request', 'name']
        fields_prototype = basic.reduceAttrs(self.fields, attrs_to_send)
        filldata_in = self.session['filldata']['in']

        for i, function in enumerate(self.view_config['on_load']):
            load_func = getattr(filldata_in, function)
            args = self.view_config['load_args'][i]
            load_data = load_func(args)
            if load_data:
                self.session['load_store'] += load_data
            # A function can return false to break the cycle of loading.
            if load_data == False:
                break

        print '---'
        interest = [154]
        print 'load_store:', self.session['load_store']
        logging.debug('data_store of interest %s', filter(lambda field: field['id'] in interest, self.session['data_store']))
        print '---'

        fields = []  # These get split up into batches
        batch_start = self.batch_id
        batch_id = 0
        batch_base = False  # Filled with max batch is batch_base is specified in view_config
        clear_loadstore = []
        for field_p in fields_prototype:
            #print 'DStore:', self.session['data_store']
            #   print 'LStore:', self.session['load_store']
            #print field_p
            flag_deleted = False
            batch_id = batch_start
            data_store_fields = filter(lambda row: row['id'] == field_p['id'], data_store)
            print data_store_fields
            #if field_p['id'] in config.fields_trued:
            #    if not data_store_fields

            if data_store_fields and not field_p['id'] in config.fields_reload and not 'dynamic' in self.view_config:  # Check if present in memory

                # This is for adding empty fields if there's a batch base, without having to delete existing RAM data
                if batch_base and not field_p['id'] in config.fields_single:
                    if len(data_store_fields) < batch_base:
                        _add = batch_base - len(data_store_fields)
                        for i in range(_add):
                            fake = copy.copy(data_store_fields[0])
                            fake['data_request'] = ''
                            data_store_fields += [fake]

                for data_store_field in data_store_fields:
                    print field_p['id'], 'in data_store'
                    field_p['data_request'] = data_store_field['data_request']
                    # Why reapply batch_id? Some unknown reason, but it does break something
                    # Because of that the batch_id must also be updated in the data_store
                    data_store_field['batch_id'] = batch_id
                    field_p['batch_id'] = batch_id
                    batch_id += 1
                    _f = field_p.copy()
                    fields.append(_f)

                #This is needed so that the fields get removed from load_store
                clear_loadstore.append(field_p['id'])
            #Delete fields if exist in data_store and view is dynamic
            elif data_store_fields and (field_p['id'] in fields_reload or 'dynamic' in self.view_config):
                print 'form DEL:', field_p['id']
                flag_deleted = True
                for data_store_field in data_store_fields:
                    data_store.remove(data_store_field)

            if not data_store_fields or field_p['id'] in fields_reload or flag_deleted:
                #if field_p['data_request']:
                print field_p['id'], 'FORM - not in data_store'
                field_p['batch_id'] = batch_id
                if field_p['data_request']:
                    func = getattr(filldata_in, field_p['data_request'])
                    field_filldata = func(field_p['id'])  # This function must return false on error and a list on success
                else:
                    field_filldata = ['']

                # Hackish solution. Check if field_filldata is as long as batch_base. If not create an empty filled array of batch_base length
                # Redo so that the data is retained and empty fields created only if there is no data
                if batch_base and not field_p['id'] in config.fields_non_base:
                    field_filldata = [y for y in field_filldata]
                    length_ff = len(field_filldata)
                    if length_ff < batch_base:
                        left = batch_base - length_ff
                        field_filldata += ['' for x in range(left)]

                for i, field_data in enumerate(field_filldata):
                    ###print field_data
                    field_p['batch_id'] = batch_id
                    _f = field_p.copy()  # Temp variable just to append to fields and then send
                    #Check if instance of datetime and convert to str
                    if isinstance(field_data, datetime.date):
                        field_data = str(field_data)
                    _f['data_request'] = field_data

                    #little hack for empties
                    f_empty = False
                    if not _f['data_request'] and _f['data_request'] != False:
                        f_empty = True
                        _f['data_request'] = ''

                    # Rewrite condition
                    if f_empty and ('dynamic' in self.view_config or field_p['id'] in config.fields_nonempty):
                        pass
                    else:
                        data_store.append({'id': field_p['id'], 'data_request': _f['data_request'], 'batch_id': field_p['batch_id']})
                        fields.append(_f)
                        batch_id += 1  # Perfect spot. Iterates through all of each kind *one kind after the other

                    if field_p['id'] in config.fields_single:
                        break

            #Getting batch_base. Need to subtract start from finish
            if 'batch_base' in self.view_config:
                if field_p['id'] == self.view_config['batch_base']:
                    batch_base = batch_id - batch_start
                    print 'batchBASE', batch_base

        # Clearing load_store
        for field_id in clear_loadstore:
            field = filter(lambda field: field['id'] == field_id, config.field_list)[0]
            if 'db_pointer' in field:
                ls_fields = filter(lambda _field: _field['id'] in field['db_pointer'], self.session['load_store'])
                for ls_field in ls_fields:
                    self.session['load_store'].remove(ls_field)

        # Adds extra fields.
        extras = self.view_config['extras']
        batch_start = batch_id
        for i in range(extras):
            for field_p in fields_prototype:
                batch_id = batch_start
                field_p['data_request'] = ''  # Cannot be None. Why? Because JS makes the object null and therefore non-existent. Python rules!!
                field_p['batch_id'] = batch_id
                fields.append(field_p)
                data_store.append({'id': field_p['id'], 'data_request': field_p['data_request'], 'batch_id': field_p['batch_id'], 'extra': True})
                batch_id += 1
        #Save batch_id
        self.batch_id = batch_id

        # Takes all the fields and splits them into batches
        allowed_fields = [field for field in fields if not field['id'] in config.fields_hidden]
        batches = []
        if allowed_fields:
            batches = basic.splitBatches(allowed_fields)

        ### Some cleanup
        self.skip_reload = []
        if 'dynamic' in self.view_config:
            self.session['load_store'] = []
        ###

        self.formSwitch()
        self.emit('log', dumps(allowed_fields))
        self.emit('fields', batches, self.current_html_el)

        #Some logging and log formatting
        _log = self.session['log']
        _log.logStore(self.session['data_store'])
        _log.log('Data Store\n' + view_id)
        _log.logStore(dumps(batches), 'Sent Fields\n')
        print '#########################################################\n'
        ###

        #Switching additional forms on. Ideally these should be children, but for the lack of thought, now it will have to be the switchAfter
        if 'switchAfter' in self.view_config:
            for view in self.view_config['switchAfter']:
                self.session['controllers']['page'].switchView(view)

    #Check requirements inside python
    def on_update(self, packet):
        print 'Update:', packet, self.session['data_store']
        filldata_out = self.session['filldata']['out']
        _packet = packet
        redo_greenlet = None

        packet = packet['args'][0]
        update_field = filter(lambda field: field['id'] == packet['id'], config.field_list)
        if update_field:
            update_field = update_field[0]
            self.formSwitch(field=update_field)
            print 'on_update', update_field
            req_check_result = self.form_check.reqCheckField(update_field, packet['batch_id'])
            if not req_check_result['result']:
                self.emit('error', 'requirements missing. save lock is now on')
                locked_at = (req_check_result['missing'], packet['batch_id'])
                if not locked_at in self.save_lock:
                    self.save_lock.append(locked_at)
                    self.pending_unlock[str(locked_at)] = _packet
            elif not self.form_check.reCheckField(packet):
                self.emit('error', 'did not pass regexp check. save lock is now on')
                locked_at = (packet['id'], packet['batch_id'])
                if not locked_at in self.save_lock_fatal:
                    self.save_lock_fatal.append(locked_at)
            else:
                if (packet['id'], packet['batch_id']) in self.save_lock_fatal:
                    self.save_lock_fatal.remove((packet['id'], packet['batch_id']))
                if not self.save_lock_fatal:
                    locked_at = (packet['id'], packet['batch_id'])
                    if locked_at in self.save_lock:
                        self.save_lock.remove((packet['id'], packet['batch_id']))
                        redo_greenlet = gevent.Greenlet(self.on_update, self.pending_unlock[str(locked_at)])
                        self.emit('log', 'Save lock is lifted')

            if not self.save_lock_fatal:
                flag_redo = False
                for i, function in enumerate(update_field['update_func']):
                    func = getattr(filldata_out, function)
                    if 'update_args' in update_field:
                        if update_field['update_args'][i]:
                            func(packet, update_field['update_args'][i])
                        else:
                            func(packet)
                    else:
                        func(packet)

                    # Essentially ['data_store'] and ['data_request'] have become a completely inflexible point in the system.
                    # The first function of any update in most cases must be the one that saves 'data_request'
                    if not flag_redo and redo_greenlet != None:
                        redo_greenlet.start()
                        redo_greenlet.join()
                        flag_redo = True
                        #Bootleg solution: need to save in case of non-fatal, but non-fatal can be fatal to some functions.
                        # Therefore the only first function executes in case of non-fatal lock

                        #if self.save_lock:
                        #    break

                        #Don't forget to get the result. If false put a save_lock on. Build mechanism so that it could also be fatal.

    def on_submit(self, refresh=False):
        ###print 'S', self.session['data_store']
        print '*****************************'
        if refresh:
            self.submit_refresh = True

        #Obsolete, but not depricated. Use exec_parent
        if 'parent' in self.view_config:
            self.formSwitch(view_id=self.view_config['parent'])

        print 'Submit', self.view_config['id']
        #self.session['data_store'],
        if not self.save_lock:
            submit = self.view_config['on_submit']
            requirements = self.view_config['req']
            filldata_out = self.session['filldata']['out']

            if 'child' in self.view_config:  # exec_child is set by execParent in filldata
                temp_view_id = self.view_config['id']
                self.exec_child = False

                for i, child_id in enumerate(self.view_config['child']):
                    if not i in self.skip_children:
                        switch = self.formSwitch(view_id=child_id)
                        if switch != False:
                            print 'submitting child', child_id, self.view_config['id']
                            self.on_submit()
                            if self.child_break:
                                break

                self.skip_children = []
                self.formSwitch(view_id=temp_view_id)
                self.exec_child = True

            #print submit, self.view_config['submit_args']
            if len(requirements) > 0:
                req_pass = self.form_check.reqCheckForm(requirements)
            else:
                req_pass = True

            print "**\t", self.view_config['id'], req_pass
            if not req_pass:
                self.emit('error', 'form is missing requirements')
                self.child_break = True
            elif self.child_break:
                self.emit('error', 'One of the sub-forms has experienced a problem. Submit aborted.')
                self.child_break = False
            else:
                #self.session['load_store'] = []

                flag_success = True
                # Removed the check (   and data_store['data_request']  )
                required_fields = filter(lambda data_store: data_store['id'] in requirements, self.session['data_store'])
                constant_view_config = self.view_config
                print 'form submit', constant_view_config
                print requirements, required_fields

                for i, function in enumerate(submit):
                    print function
                    args = constant_view_config['submit_args'][i]
                    func = getattr(filldata_out, function)
                    result = func(required_fields, args)
                    if result is False:
                        self.emit('error', 'error submitting form')
                        flag_success = False
                        break

                print '__loadstore submit', self.session['load_store']
                # Prevent children from refreshing.
                if self.submit_refresh and self.exec_child:
                    self.submit_refresh = False
                    print 'submit refersh', self.view_config['id']
                    self.session['controllers']['page'].switchView(self.view_config['id'])
                    #Also referesh ALL kids. It makes sense. Trust me.
                    if 'child' in self.view_config:
                        for child in self.view_config['child']:
                            self.session['controllers']['page'].switchView(child)

        else:
            self.emit('error', 'save lock is on. check requirements')

    # If field id is none stores fields and view config. Otherwise retrieves them
    def formSwitch(self, field=None, view_id=None):
        print 'formSwitch'
        if not field and not view_id:
            view_id = self.view_config['id']
            self.view_store[view_id] = [self.view_config, self.fields]
            self.current = view_id
            print view_id
        elif view_id:
            print view_id, self.view_store, self.view_store.keys()
            if view_id in self.view_store.keys():
                self.view_config, self.fields = self.view_store[view_id]
                self.current = view_id
                print '-', view_id
            else:
                return False
        else:
            view_id = field['view']
            print '--', view_id, self.current
            if not self.current in view_id:
                if len(view_id) == 1:
                    self.view_config, self.fields = self.view_store[view_id[0]]
                    self.current = view_id[0]
                else:
                    #Parse through the view history (page control) and find the most recent one.
                    #print self.session['controllers']['page'].view_history[::-1]
                    for view in self.session['controllers']['page'].view_history[::-1]:
                        if view in view_id:
                            self.view_config, self.fields = self.view_store[view]
                            self.current = view
                            break

    def on_upload(self, file, packet):
        #Packet is a list of all the fields in a batch. Each is a dict
        if not 'upload' in self.view_config:
            print 'ERROR. Upload requested not configured'
            return

        #Modify packet to pass to saveDS
        fake_packet = packet[0]
        fake_packet['id'], fake_packet['data_request'] = self.view_config['upload'], file

        self.session['filldata']['out'].saveDataStore(fake_packet)