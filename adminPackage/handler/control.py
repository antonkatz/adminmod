from gevent import monkey; monkey.patch_all()
import gevent

from socketio.namespace import BaseNamespace
from ..converter import basic
from .. import config


class Control(BaseNamespace):
    def recv_connect(self):
        self.request['gevent_group'].add(gevent.spawn(self.init))
    
    def init(self):
        self.request['gevent_event'].get()
        
        self.session['endpoints']['control'] = self

    def on_load(self, view_id):
        self.controls = filter(lambda control: control['view'] == view_id, config.control_list)
        attrs_to_send = ['id', 'name']
        controls = basic.reduceAttrs(self.controls, attrs_to_send)
        self.emit('controls', controls, self.current_html_el)
        
    def on_controlRequest(self, control_id, args=None):
        _control = filter(lambda control: control['id'] == control_id, config.control_list)
        print '..........', control_id, args
        control = None
        if control_id in self.session['controllers']['page'].pinned_view:
            pinned_view = self.session['controllers']['page'].pinned_view[control_id]
            control = filter(lambda control: control['view'] == pinned_view, _control)[0]
            del self.session['controllers']['page'].pinned_view[control_id]
        else:
            if len(_control) > 1:
                flag_break = False

                #For switching to correct view
                view_list = self.session['controllers']['page'].view_history[::-1]
                if args:
                    if 'id' in args[0]:
                        field = filter(lambda field: field['id'] == args[0]['id'], config.field_list)[0]
                        view_list = field['view']

                for view in view_list:
                    for _c in _control:
                        if _c['view'] == view:
                            self.session['endpoints']['form'].formSwitch(view_id=view)
                            control = _c
                            flag_break = True
                            break
                    if flag_break:
                        break
            else:
                control = _control[0]

        if control:
            control_submit = control['on_submit']
            for on_submit_data in control_submit:
                c_controller = on_submit_data['controller']
                c_method = on_submit_data['method']
                c_args = on_submit_data['args']
                #print 'control', c_args, args, c_method
                selected_controller = self.session['controllers'][c_controller]
                selected_method = getattr(selected_controller, c_method)
                if args:
                    selected_method(c_args, args)
                else:
                    selected_method(c_args)