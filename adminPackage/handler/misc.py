from gevent import monkey; monkey.patch_all()
import gevent

from socketio.namespace import BaseNamespace
from ..controller.page import PageControl
from ..controller.form import FormControl
from ..controller.comms import CommsControl
from ..controller.box import BoxControl
#from ..check import connection
from .. import config
from ..storage import pg
from _logging import LOG


class Page(BaseNamespace):
    def recv_connect(self):
        self.request['gevent_group'].add(gevent.spawn(self.init))
    
    def init(self):
        self.request['gevent_event'].get()
        
        self.session['endpoints']['page'] = self
        self.page_control = PageControl(self.session['endpoints'])
        self.session['controllers']['page'] = self.page_control
        
    def initView(self):
        for view in config.initial_views:
            self.page_control.switchView(view)

    def recv_disconnect(self):
        try:
            self.session['pg_storage'].con.close()
        except:
            pass
        self.disconnect()

        
class initCon(BaseNamespace):
    def recv_connect(self):
        print 'LOG:', 'initCon\t recv_connect'
        self.session['log'] = LOG()
        self.session['endpoints'] = {}
        self.session['controllers'] = {}
        self.session['endpoints']['initcon'] = self

        self.session['pg_storage'] = pg.storage()

        self.request['gevent_event'].set()
        self.request['gevent_group'].join()

        self.session['controllers']['form'] = FormControl(self.session)
        self.session['controllers']['comms'] = CommsControl(self.session)
        self.session['controllers']['box'] = BoxControl(self.session, self.request)
        self.session['endpoints']['page'].initView()

