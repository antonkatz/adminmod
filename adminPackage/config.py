import settings_local
#I don't like this but it's fine

view_list = [{'id': 'top_wrap', 'req': [], 'on_submit': None, 'type': None, 'html_el': 'div_body', 'extras': 0},
             {'id': 'order', 'req': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': [], 'load_args': []},
             {'id': 'order_edit', 'req': [1, 4, 36, 27,     7,8     ], 'shift_dsTQ': 9,
                  'on_submit': ['selectPreffered', 'saveDB', 'generateBlog'],
                  'submit_args': [{'query_set_id': 'order_client_price', 'replace': 5}, {'query_set_id': 'order_save'}, {'slug': 4, 'title': 151, 'description': 152, 'bools': [147, 148, 149, 150],
                                                                                                                         'store_path': 'frontend/blogs/', 'address': 1, 'music': 154, 'bool_music': 157,
                                                                                                                        'order_id': 20}],
                  'type': 'form', 'html_el': 'order_edit', 'extras': 0, 'child': ['order_client_list', 'address_split'],
                  'on_load': ['pickle', 'requestDB', 'separateData', 'default', 'default', 'default', 'listMusic', '_switchView'], 'prefetch': {'order_save.order_id': 20},
                  'load_args': [{'query_set_id': '_settings_packages_load'}, {'query_set_id': 'order_load'},
                                {'pointers': ['order_load.client_id', 'order_load.template_id', 'order_load.qty']},
                                {'fields': ['order_load.bool_photos', 'order_load.bool_panorama', 'order_load.bool_map', 'order_load.bool_streetview'], 'default': [True]},
                                {'fields': ['order_load.bool_music'], 'default': [False]},
                                {'fields': ['order_load.appointment_date'], 'default': ['2012.12.12']}, {'music_folder': 1192996495, 'name_pointer': 'music_names', 'data_pointer': 'music_links'},
                                {'view_id': 'order_edit_menu'}]},
             {'id': 'order_selector', 'req': [], 'on_submit': [], 'type': 'form', 'html_el': 'order_selector', 'extras': 0, 'parent': 'order_edit',
                  'on_load': ['requestDB', 'requestDB', 'mergeLoadStore'],
                  'load_args': [{'query_set_id': 'order_select_byslug'}, {'query_set_id': 'order_select_byclient'}, {'target': ['order_client_select', 'order_client_pin_'], 'control': 'order_slug_select', 'compare': 'order_id'}],
                  'dynamic': True},
             {'id': 'order_client_list', 'req': [35], 'on_submit': ['transferCollapsed', 'execParent'], 'submit_args': [{'from': [35], 'to': [36]}, 'order_edit'],
                  'type': 'form', 'html_el': 'order_client_list', 'extras': 0, 'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'order_load_clients_data'}]},
             {'id': 'order_details', 'parent': 'order_edit', 'req': [], 'on_submit': None, 'type': None, 'html_el': 'order_details', 'extras': 0},
             {'id': 'order_edit_menu', 'req': [], 'on_submit': None, 'type': 'control', 'html_el': 'order_edit_menu', 'extras': 0, 'on_load': []},

             {'id': 'email_selector', 'req': [], 'on_submit': None, 'type': 'form', 'html_el': 'email_selector', 'extras': 0,
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'email_select'}]},
             {'id': 'email_recipient', 'req': [], 'on_submit': None, 'type': 'form', 'html_el': 'order_edit', 'extras': 0,
                  'on_load': ['requestDB', 'separateData'], 'load_args': [{'query_set_id': 'email_recipient'}, {'pointers': ['email_recipient.email']}]},

             {'id': 'address_split', 'req': [30, 28, 3, 32, 31, 29, 33], 'on_submit': ['execParent', 'saveDB'], 'submit_args': ['order_edit', {'query_set_id': 'order_save_address_comp'}],
                  'type': 'form', 'html_el': 'address_split', 'extras': 0, 'on_load': ['addressSplit'], 'load_args': [{'address': 1}]},

             {'id': 'order_templates', 'req': [37], 'child': ['template_order_selector'], 'on_submit': ['transferCollapsed', 'saveDB', 'saveDB', 'newOrderBrochure'], 'shift_dsTQ': 20,
                  'submit_args': [{'from': [37], 'to': [39]}, {'query_set_id': 'template_save'}, {'query_set_id': 'print_job_delete'}, {'order_id': 20}],
                  'type': 'form', 'html_el': 'order_edit', 'extras': 0, 'on_load': ['requestDB', 'requestDB'], 'load_args': [{'query_set_id': 'template_pin'}, {'query_set_id': 'template_link_pin'}]},
             {'id': 'template_order_selector', 'req': [], 'type': 'form', 'html_el': 'template_order_selector', 'extras': 0, 'on_submit': ['execParent'], 'submit_args': ['order_templates'],
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'template_list'}], 'dynamic': True},

             {'id': 'order_invoice', 'req': [], 'child': ['order_invoice_additional', 'order_invoice_client'],
                  'on_submit': ['saveDB'], 'submit_args': [{'query_set_id': 'invoice_save'}],
                  'type': 'form', 'html_el': 'order_edit', 'extras': 0, 'on_load': ['requestDB', 'separateData'], 'load_args': [{'query_set_id': 'invoice_load'}, {'pointers': ['invoice_load.template_id']}]},
             {'id': 'order_invoice_additional', 'req': [45],
                  'on_submit': ['transferCollapsed', 'requestDBlink', 'execParent'], 'submit_args': [{'from': [45], 'to': [47]}, {'query_set_id': 'print_job_update_qty'}, 'order_invoice'], 'batch_base': 122,
                  'type': 'form', 'html_el': 'order_invoice_additional', 'extras': 0, 'on_load': ['requestDB', 'separateData'], 'shift_dsTQ': 20,
                  'load_args': [{'query_set_id': 'template_pin_'}, {'pointers': ['invoice_load.qty']}]},
             {'id': 'order_invoice_client', 'req': [49], 'on_submit': ['transferCollapsed', 'execParent'], 'submit_args': [{'from': [49], 'to': [50]}, 'order_invoice'],
                  'type': 'form', 'html_el': 'order_invoice_client', 'extras': 0, 'on_load': ['separateData'], 'load_args': [{'pointers': ['invoice_load.split']}], 'batch_base': 0},

             {'id': 'order_printer', 'req': [], 'on_submit': ['saveDB', 'saveDB'], 'submit_args': [{'query_set_id': 'printer_save'}, {'query_set_id': 'print_update_printer'}],
                  'type': 'form', 'html_el': 'order_edit', 'extras': 0, 'child': [],
                  'on_load': ['requestDB', 'requestDB', 'requestDB'], 'load_args': [{'query_set_id': 'printer_list'}, {'query_set_id': 'printer_load'}, {'query_set_id': 'order_client_printer'}]},
             {'id': 'address_split_map_p', 'req': [], 'on_submit': ['execParent'], 'submit_args': ['order_printer'],
                  'type': 'form', 'html_el': 'address_split_map_p', 'extras': 0, 'on_load': ['addressSplit'], 'load_args': [{'address': 120}]},

             {'id': 'client', 'req': [], 'on_submit': [], 'submit_args': [],
                  'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': [], 'load_args': []},
             {'id': 'client_edit', 'req': [10, 11, 16], 'type': 'form', 'html_el': 'client_edit', 'extras': 0, 'on_submit': ['saveDB'], 'submit_args': [{'query_set_id': 'client_save'}],
                  'child': ['client_email'], 'on_load': ['requestDB', 'requestDB'], 'load_args': [{'query_set_id': 'client_load'}, {'query_set_id': 'printer_list'}],
                  'switchAfter': ['client_email', '_settings_packages_client'], 'prefetch': {'client_save.client_id': 12}},
             {'id': 'client_selector', 'req': [], 'type': 'form', 'html_el': 'client_selector', 'extras': 0,
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'client_select'}], 'dynamic': True},
             {'id': 'client_email', 'req': [26], 'on_submit': ['transferCollapsed', 'execParent'], 'submit_args': [{'from': [26], 'to': [123]}, 'client_edit'],
                  'type': 'form', 'html_el': 'client_email', 'extras': 1, 'on_load': ['separateData'], 'load_args': [{'pointers': ['client_load.email']}]},
             {'id': '_settings_packages_client', 'req': [7, 8], 'on_submit': ['execParent'], 'submit_args': ['client_edit'],
                  'type': 'form', 'html_el': '_settings_packages_client', 'extras': 0,
                  'on_load': ['pickle', 'requestDB', 'replacePickle'], 'load_args': [{'query_set_id': '_settings_packages_load'}, {'query_set_id': 'client_price_load'},
                                                                    {'compare': [7, 'client_price_load.package'], 'replace': [8, 'client_price_load.price']}]},

             {'id': 'brokerages', 'req': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': [], 'load_args': []},
             {'id': 'brokerages_selector', 'req': [56], 'on_submit': [], 'submit_args': [], 'type': 'form', 'html_el': 'brokerages_selector', 'extras': 0, 'parent': 'brokerages_details',
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'brokerages_list'}], 'dynamic': True},
             {'id': 'brokerages_details', 'req': [64], 'on_submit': ['saveDB'], 'submit_args': [{'query_set_id': 'brokerages_save'}], 'type': 'form', 'html_el': 'brokerages_details', 'prefetch': {'brokerages_save.brokerage_id': 57},
                  'extras': 0, 'upload': 64, 'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'brokerages_pin'}]},

             {'id': 'user', 'req': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': [], 'load_args': []},
             {'id': 'user_role_selector', 'req': [], 'on_submit': [], 'submit_args': [], 'type': 'form', 'html_el': 'user_role_selector', 'extras': 0, 'parent': 'user_edit',
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'user_role_list'}], 'dynamic': True},
             {'id': 'user_selector', 'req': [], 'on_submit': [], 'submit_args': [], 'type': 'form', 'html_el': 'user_selector', 'extras': 0, 'parent': 'user_edit',
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'user_list'}], 'shift_dsTQ': 72, 'dynamic': True},
             {'id': 'user_edit', 'req': [], 'type': 'form', 'html_el': 'user_edit', 'extras': 0, 'on_load': ['requestDB', 'requestDB'], 'load_args': [{'query_set_id': 'user_load'}, {'query_set_id': 'user_role_list'}],
                  'on_submit': ['default', 'saveDB'], 'submit_args': [{'id': [91], 'value': False}, {'query_set_id': 'user_save'}], 'prefetch': {'user_save.user_id': 73},},

             {'id': 'suborder_selector', 'req': [], 'on_submit': ['execParent'], 'submit_args': ['order_edit'],
                  'type': 'form', 'html_el': 'suborder_selector', 'extras': 0, 'parent': 'suborder_edit',
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'suborder_list'}], 'dynamic': True},
             {'id': 'suborder_details', 'parent': 'suborder_templates', 'req': [],
                  'on_submit': [], 'type': 'form', 'html_el': 'suborder_details', 'extras': 0,
                  'on_load': ['requestDB', 'separateData'], 'load_args': [{'query_set_id': 'suborder_load'}, {'pointers': ['suborder_load.template_id']}]},
             {'id': 'suborder_edit_menu', 'req': [], 'on_submit': [], 'type': 'control', 'html_el': 'suborder_edit_menu', 'extras': 0, 'on_load': []},

             {'id': 'suborder_invoice', 'req': [], 'on_submit': ['requestDBlink', 'saveDB'], 'submit_args': [{'query_set_id': 'subprint_update_qty_'}, {'query_set_id': 'subinvoice_save'}],
                  'child': ['suborder_invoice_client', 'suborder_invoice_additional'],
                  'type': 'form', 'html_el': 'suborder_edit', 'extras': 0, 'on_load': ['requestDB', 'separateData'], 'load_args': [{'query_set_id': 'subinvoice_load'}, {'pointers': ['subinvoice_load.template_id']}, ],
                  'shift_dsTQ': [20, 94]},
             {'id': 'suborder_invoice_client', 'req': [103], 'on_submit': ['transferCollapsed', 'execParent'], 'submit_args': [{'from': [103], 'to': [104], 'nunique': [103]}, 'suborder_invoice'],
                  'type': 'form', 'html_el': 'suborder_invoice_client', 'extras': 0, 'on_load': ['separateData'], 'load_args': [{'pointers': ['subinvoice_load.split']}], 'batch_base': 0},
             {'id': 'suborder_invoice_additional', 'req': [105, 107], 'on_submit': ['default', 'transferCollapsed', 'execParent'], 'submit_args': [{'id': [107], 'value': 0}, {'from': [105, 107], 'to': [106, 108],
                                                                                                                                                                       'nunique': [105, 107]}, 'suborder_invoice'],
                  'type': 'form', 'html_el': 'suborder_invoice_additional', 'extras': 0, 'on_load': ['requestDB', 'separateData'], 'shift_dsTQ': [20],
                  'load_args': [{'query_set_id': 'subinvoice_subtotal_load'}, {'pointers': ['sub_template_pin_.name', 'subinvoice_load.qty']}]},

             {'id': 'suborder_templates', 'req': [102, 98], 'child': ['template_suborder_selector'], 'on_submit': ['default', 'transferCollapsed', 'default', 'saveDB', 'requestDBlink', 'saveDB'],
                  'submit_args': [{'id': [98], 'value': 0}, {'from': [102, 98], 'to': [97, 99], 'nunique': [98]}, {'id': [97], 'value': '[]'}, {'query_set_id': 'suborder_template_save'},
                                  {'query_set_id': 'subprint_job_save'}, {'query_set_id': 'subprint_job_delete'}],
                  'type': 'form', 'html_el': 'suborder_edit', 'extras': 0, 'batch_base': 102, 'shift_dsTQ': [94, 20],
                  'on_load': ['requestDB', 'requestDB', 'separateData'], 'load_args': [{'query_set_id': 'sub_template_pin'}, {'query_set_id': 'template_load'}, {'pointers': ['template_load.qty']}]},
             {'id': 'template_suborder_selector', 'req': [], 'type': 'form', 'html_el': 'template_suborder_selector', 'extras': 0, 'on_submit': ['execParent'], 'submit_args': ['suborder_templates'],
                  'on_load': ['requestDB'], 'load_args': [{'query_set_id': 'template_list'}], 'dynamic': True, 'shift_dsTQ': [94, 20]},

             {'id': 'suborder_printer', 'req': [], 'on_submit': ['saveDB', 'requestDBlink'], 'submit_args': [{'query_set_id': 'subprinter_save'}, {'query_set_id': 'subprint_update_printer'}], 'child': [],
              'shift_dsTQ': [20, 94],
                  'type': 'form', 'html_el': 'suborder_edit', 'extras': 0, 'on_load': ['requestDB', 'requestDB'], 'load_args': [{'query_set_id': 'subprinter_load'}, {'query_set_id': 'printer_list'}]},
             {'id': 'address_split_map_sp', 'req': [], 'on_submit': ['execParent'], 'submit_args': ['suborder_printer'],
                  'type': 'form', 'html_el': 'address_split_map_sp', 'extras': 0, 'on_load': ['addressSplit'], 'load_args': [{'address': 115}]},


             {'id': 'master_retoucher', 'req': [], 'on_submit': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': ['requestDB'],
              'load_args': [{'query_set_id': 'retoucher_list'}], 'dynamic': True},
             {'id': 'master_printer', 'req': [], 'on_submit': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': ['requestDB'],
              'load_args': [{'query_set_id': '__printer_list'}], 'dynamic': True},
             {'id': 'templates', 'req': [], 'on_submit': [], 'type': 'form', 'html_el': 'page', 'extras': 0, 'on_load': ['parseNewBrochures', 'requestDB', 'default'],
              'load_args': [{'args': None}, {'query_set_id': '__template_list'},
                            {'fields': ['__template_list.prefix'], 'default': [settings_local.server_brochure_access_path]}], 'dynamic': True},


             {'id': 'main_menu', 'req': [], 'on_submit': None, 'type': 'control', 'html_el': 'main_menu', 'extras': 0, 'on_load': []},
             {'id': 'order_list', 'req': [], 'on_submit': None, 'type': 'control', 'html_el': 'div_body', 'extras': 0},
             {'id': '_settings_packages', 'req': [7, 8], 'on_submit': ['pickle'], 'submit_args': [{'query_set_id': '_settings_packages_save'}],
                  'type': 'form', 'html_el': 'page', 'extras': 1, 'on_load': ['pickle'], 'load_args': [{'query_set_id': '_settings_packages_load'}]},
             ]
#Transfer collapsed takes ALL required_fields (passed from on_submit) and collapses them into one. Means it needs required_fields specified
#The batches in form check are split based of max batch_id gotten from the first requirement
#Type can be form, control, or None. None means that the template will be loaded, but nothing else.
#view_active_exclude
#
#{'view_id': 'address_split', 'check': ['order_load.address'], 'stop': [1]}
#{'query_set_id': 'print_job_save'} 'requestDBlink'


#Clearing is for load_store

#Fields that overlap in forms must be hidden?
# 'quickSearch', {'query_set_id': 'order_select'},
#Qucik search has no use if the view gets the data on load

#current max: 162
#
# clearFields {'fields': [12], 'hard': False}
field_list = [{'id': 18, 'name': 'Search', 'view': ['order'], 'update_func': ['saveDataStore', '_switchView'],
               'update_args': [None, {'view_id': 'order_selector'}], 'update_req': [None], 'data_request': '', 're': '[A-Za-z0-9]+'},

              {'id': 20, 'name': 'order_id', 'view': ['order_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore',
               'db_pointer': ['order_client_select.order_id', 'order_slug_select.order_id'], 're': '[A-Za-z]+'},
              {'id': 21, 'name': 'slug', 'view': ['order_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_select.slug', 'order_slug_select.slug'], 're': '[A-Za-z]+'},
              {'id': 19, 'name': 'First name', 'view': ['order_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_pin_.fname', 'order_client_pin.fname'], 're': '[A-Za-z]+'},
              {'id': 22, 'name': 'Last name', 'view': ['order_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_pin_.lname', 'order_client_pin.lname'], 're': '[A-Za-z]+'},

              {'id': 0, 'name': 'first name', 'view': ['order_client_list', 'order_invoice_client', 'suborder_invoice_client'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_repin.fname']},
              {'id': 23, 'name': 'last name', 'view': ['order_client_list', 'order_invoice_client', 'suborder_invoice_client'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_repin.lname']},
              {'id': 34, 'name': 'email', 'view': ['order_client_list'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_client_repin.email']},
              {'id': 35, 'name': 'client_id', 'view': ['order_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.client_id']},
              {'id': 36, 'name': 'client_id_collapsed', 'view': ['order_edit'], 'update_func': [], 'update_req': [], 'data_request': '', 'db_pointer': []},

              {'id': 1, 'name': 'address', 'view': ['order_edit'], 'update_func': ['saveDataStore', 'removeFields', '_switchView'],
               'update_args': [None, {'fields': [2, 30, 28, 3, 32, 31, 29, 33]}, {'view_id': 'address_split'}], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.user_address']},
              {'id': 4, 'name': 'blog slug', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.slug']},
              {'id': 27, 'name': 'appointment date', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 're': '[0-9-.]{8,}', 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.appointment_date'],
               'convert': True},
              {'id': 9, 'name': 'default_package_option', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.package']},
              {'id': 5, 'name': 'package', 'view': ['order_edit', 'order_invoice'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 7, 'data': 8, 'selected': 9}},
              {'id': 147, 'name': 'photos', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.bool_photos']},
              {'id': 148, 'name': 'panorama', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.bool_panorama']},
              {'id': 149, 'name': 'map', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.bool_map']},
              {'id': 150, 'name': 'streetview', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.bool_streetview']},
              {'id': 157, 'name': 'music on/off', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.bool_music']},
              {'id': 153, 'name': 'default_music_option', 'view': ['order_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.music']},
              {'id': 154, 'name': 'music', 'view': ['order_edit'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 156, 'data': 155, 'selected': 153}},
              {'id': 155, 'name': 'music_data', 'view': ['order_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['music_links']},
              {'id': 156, 'name': 'music_display', 'view': ['order_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['music_names']},
              {'id': 151, 'name': 'title', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.title']},
              {'id': 152, 'name': 'description', 'view': ['order_edit'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.description']},

              {'id': 2, 'name': 'unit', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 're': '[0-9]*', 'data_request': 'requestLoadStore', 'db_pointer': ['subpremise', 'order_load.unit']},
              {'id': 30, 'name': 'street number', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['street_number', 'order_load.street_number']},
              {'id': 28, 'name': 'street', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['route', 'order_load.street']},
              {'id': 3, 'name': 'town', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['locality', 'order_load.town']},
              {'id': 32, 'name': 'province', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['administrative_area_level_1', 'order_load.province']},
              {'id': 31, 'name': 'country', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['country', 'order_load.country']},
              {'id': 29, 'name': 'postal code', 'view': ['address_split'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['postal_code', 'order_load.postal_code']},
              {'id': 33, 'name': 'map url', 'view': ['address_split'], 'update_func': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['map_url']},

              {'id': 37, 'name': 'template', 'view': ['order_edit', 'order_templates'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.template_id']},
              {'id': 38, 'name': 'template name', 'view': ['order_templates'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_pin.name']},
              {'id': 39, 'name': 'template_id_collapsed', 'view': ['order_templates'], 'update_func': [], 'update_req': [], 'data_request': '', 'db_pointer': []},
              {'id': 40, 'name': 'template_id', 'view': ['template_order_selector'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_list.id']},
              {'id': 41, 'name': 'template_name', 'view': ['template_order_selector'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_list.name']},
              {'id': 161, 'name': 'template_link', 'view': ['order_templates'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_link_pin.template_links']},

              {'id': 102, 'name': 'template', 'view': ['suborder_details', 'suborder_templates'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['suborder_load.template_id']},
              {'id': 96, 'name': 'template name', 'view': ['suborder_templates'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['sub_template_pin.name']},
              {'id': 97, 'name': 'template_id_collapsed', 'view': ['suborder_templates'], 'update_func': [], 'update_req': [], 'data_request': '', 'db_pointer': []},
              {'id': 98, 'name': 'qty', 'view': ['suborder_templates'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_load.qty']},
              {'id': 99, 'name': 'qty_collapsed', 'view': ['suborder_templates'], 'update_func': [], 'update_req': [], 'data_request': '', 'db_pointer': []},
              {'id': 100, 'name': 'template_id', 'view': ['template_suborder_selector'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_list.template_id']},
              {'id': 101, 'name': 'template_name', 'view': ['template_suborder_selector'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_list.name']},

              {'id': 42, 'name': 'price', 'view': ['order_invoice'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['invoice_load.price']},
              {'id': 44, 'name': 'shipping cost', 'view': ['order_invoice'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['invoice_load.shipping']},
              {'id': 122, 'name': 'template', 'view': ['order_invoice_additional'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['template_pin_.name']},
              {'id': 129, 'name': 'template_id', 'view': ['order_invoice', 'order_invoice_additional'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['invoice_load.template_id']},
              {'id': 45, 'name': 'quantity', 'view': ['order_invoice_additional'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['invoice_load.qty']},
              {'id': 47, 'name': 'quantity_collapsed', 'view': ['order_invoice'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},
              {'id': 49, 'name': 'percentage', 'view': ['order_invoice_client'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['invoice_load.split']},
              {'id': 50, 'name': 'percentage_collapsed', 'view': ['order_invoice'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},

              {'id': 110, 'name': 'shipping cost', 'view': ['suborder_invoice'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subinvoice_load.shipping']},
              {'id': 111, 'name': 'template', 'view': ['suborder_invoice_additional'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['sub_template_pin_.name']},
              {'id': 146, 'name': 'template_id', 'view': ['suborder_invoice_additional'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subinvoice_load.template_id']},
              {'id': 105, 'name': 'quantity', 'view': ['suborder_invoice_additional'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subinvoice_load.qty']},
              {'id': 106, 'name': 'quantity_collapsed', 'view': ['suborder_invoice'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},
              {'id': 107, 'name': 'price', 'view': ['suborder_invoice_additional'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subinvoice_subtotal_load.price']},
              {'id': 108, 'name': 'price_collapsed', 'view': ['suborder_invoice'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},
              {'id': 103, 'name': 'percentage', 'view': ['suborder_invoice_client'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subinvoice_load.split']},
              {'id': 104, 'name': 'percentage_collapsed', 'view': ['suborder_invoice'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},

              {'id': 51, 'name': 'default_printer_option', 'view': ['order_printer'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['order_load.printer', 'order_client_printer.printer_id']},
              {'id': 52, 'name': 'printer', 'view': ['order_printer'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 54, 'data': 53, 'selected': 51}},
              {'id': 53, 'name': 'printer_data', 'view': ['order_printer'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.printer_id']},
              {'id': 54, 'name': 'printer_display', 'view': ['order_printer'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.name']},
              {'id': 119, 'name': 'shipping', 'view': ['order_printer'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_load.ship']},
              {'id': 120, 'name': 'address', 'view': ['order_printer'], 'update_func': ['saveDataStore', 'removeFields', '_switchView'],
               'update_args': [None, {'fields': [121]}, {'view_id': 'address_split_map_p'}], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_load.shipping_address']},
              {'id': 121, 'name': 'map url', 'view': ['address_split_map_p'], 'update_func': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['map_url']},

              {'id': 117, 'name': 'default_printer_option', 'view': ['suborder_details'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['suborder_load.printer_id']},
              {'id': 112, 'name': 'printer', 'view': ['suborder_printer'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 114, 'data': 113, 'selected': 117}},
              {'id': 113, 'name': 'printer_data', 'view': ['suborder_printer'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.printer_id']},
              {'id': 114, 'name': 'printer_display', 'view': ['suborder_printer'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.name']},
              {'id': 116, 'name': 'shipping', 'view': ['suborder_printer'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subprinter_load.ship']},
              {'id': 115, 'name': 'address', 'view': ['suborder_printer'], 'update_func': ['saveDataStore', 'removeFields', '_switchView'],
               'update_args': [None, {'fields': [118]}, {'view_id': 'address_split_map_sp'}], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['subprinter_load.shipping_address']},
              {'id': 118, 'name': 'map url', 'view': ['address_split_map_sp'], 'update_func': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['map_url']},


              {'id': 94, 'name': 'suborder_id', 'view': ['suborder_selector'], 'update_func': [None], 'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore',
               'db_pointer': ['suborder_list.suborder_id']},
              {'id': 95, 'name': 'date', 'view': ['suborder_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['suborder_list.date']},


              {'id': 55, 'name': 'Search', 'view': ['brokerages'], 'update_func': ['saveDataStore', '_switchView'],
               'update_args': [None, {'view_id': 'brokerages_selector'}], 'update_req': [None], 'data_request': '', 're': '[A-Za-z0-9]+'},
              {'id': 57, 'name': 'brokerage_id', 'view': ['brokerages_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.brokerage_id'], 're': '[A-Za-z]+'},
              {'id': 66, 'name': 'logo_file', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.logo']},
              {'id': 56, 'name': 'name', 'view': ['brokerages_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.name'], 're': '[A-Za-z]+'},
              {'id': 67, 'name': 'address', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.address']},
              {'id': 68, 'name': 'phone', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.phone']},
              {'id': 69, 'name': 'fax', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.fax']},
              {'id': 70, 'name': 'site', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.site']},
              {'id': 71, 'name': 'notes', 'view': ['brokerages_selector'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_list.notes']},

              {'id': 58, 'name': 'name', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.name']},
              {'id': 59, 'name': 'address', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.address']},
              {'id': 60, 'name': 'phone', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.phone']},
              {'id': 61, 'name': 'fax', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.fax']},
              {'id': 62, 'name': 'site', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.site']},
              {'id': 65, 'name': 'notes', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.notes']},
              {'id': 63, 'name': 'logo', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': None, 'db_pointer': []},
              {'id': 64, 'name': 'logo_file', 'view': ['brokerages_details'], 'update_func': ['saveDataStore'], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['brokerages_pin.logo']},

              {'id': 72, 'name': 'Search', 'view': ['user'], 'update_func': ['saveDataStore', '_switchView'],
               'update_args': [None, {'view_id': 'user_selector'}], 'update_req': [None], 'data_request': '', 're': '[A-Za-z0-9]+'},
              {'id': 73, 'name': 'user_id', 'view': ['user_selector'], 'update_func': [None], 'update_args': [None], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['user_list.user_id'], 're': '[A-Za-z]+'},
              {'id': 74, 'name': 'user name', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_list.user_name'], 're': '[A-Za-z]+'},
              {'id': 75, 'name': 'name', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_list.name'], 're': '[A-Za-z]+'},
              {'id': 90, 'name': 'name', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_list.address'], 're': '[A-Za-z]+'},
              {'id': 76, 'name': 'position', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['_user_role_list.name'], 're': '[A-Za-z]+'},
              {'id': 78, 'name': 'phone', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_list.phone'], 're': '[A-Za-z]+'},
              {'id': 77, 'name': 'email', 'view': ['user_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_list.email']},

              {'id': 79, 'name': 'name', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.name']},
              {'id': 86, 'name': 'user name', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.user_name']},
              {'id': 87, 'name': 'password', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.password']},
              {'id': 93, 'name': 'default_position_option', 'view': ['user_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.role']},
              {'id': 80, 'name': 'position', 'view': ['user_edit'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 89, 'data': 88, 'selected': 93}},
              {'id': 81, 'name': 'phone', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.phone']},
              {'id': 82, 'name': 'email', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.email']},
              {'id': 83, 'name': 'site', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.site']},
              {'id': 84, 'name': 'skype', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.skype']},
              {'id': 85, 'name': 'notes', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.notes']},
              {'id': 91, 'name': 'activation status', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.status']},
              {'id': 92, 'name': 'address', 'view': ['user_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_load.address']},

              {'id': 88, 'name': 'role_id', 'view': ['user_role_selector', 'user_edit'], 'update_func': [None], 'update_args': [None], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['user_role_list.role_id']},
              {'id': 89, 'name': 'role', 'view': ['user_role_selector', 'user_edit'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['user_role_list.name']},


              {'id': 7, 'name': 'package name', 'view': ['_settings_packages', '_settings_packages_client'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [], 'data_request': 'requestLoadStore'},
              {'id': 8, 'name': 'package price', 'view': ['_settings_packages', '_settings_packages_client'], 'update_func': ['saveDataStore', 'updateDB'],
               'update_args': [None, {'view_id': '_settings_packages_client', 'fields': [7,8], 'query_set_id': 'client_package_save'}], 'update_req': [7], 'data_request': 'requestLoadStore', 're': '[0-9]+$'},


              {'id': 17, 'name': 'Search', 'view': ['client'], 'update_func': ['saveDataStore', '_switchView'],
               'update_args': [None, {'view_id': 'client_selector'}], 'update_req': [None], 'data_request': '', 're': '[A-Za-z0-9]+'},

              {'id': 10, 'name': 'First name', 'view': ['client_edit'], 'update_func': ['saveDataStore'],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['client_load.fname'], 're': '[A-Za-z]+'},
              {'id': 11, 'name': 'Last name', 'view': ['client_edit'], 'update_func': ['saveDataStore'], 're': '[a-zA-Z]+',
               'update_args': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['client_load.lname']},
              {'id': 16, 'name': 'phone', 'view': ['client_edit'], 'update_func': ['saveDataStore'], 're': '[0-9]+', 'update_args': [None], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['client_load.phone']},
              {'id': 26, 'name': 'email', 'view': ['client_email'], 'update_func': ['saveDataStore'], 're': '[0-9@a-zA-Z.]+', 'update_args': [None], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['client_load.email']},
              {'id': 123, 'name': 'email_collapsed', 'view': ['client_edit'], 'update_func': [], 'update_req': [], 'data_request': None, 'db_pointer': []},


              {'id': 13, 'name': 'First name', 'view': ['client_selector'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['client_select.fname'], 're': '[A-Za-z]+'},
              {'id': 14, 'name': 'Last name', 'view': ['client_selector'], 'update_func': [None], 're': '[a-zA-Z]+',
               'update_args': [None], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['client_select.lname']},
              {'id': 12, 'name': 'client_id', 'view': ['client_selector'], 'update_func': [], 'update_args': [], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['client_select.client_id']},
              {'id': 15, 'name': 'phone', 'view': ['client_selector'], 'update_func': [], 'update_args': [], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['client_select.phone']},
              {'id': 125, 'name': 'default_printer_option', 'view': ['client_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['client_load.printer_id']},
              {'id': 126, 'name': 'default printer', 'view': ['client_edit'], 'update_func': ['saveDataStore_option'], 'update_req': [], 'data_request': 'requestOptions',
               'data_args': {'display': 128, 'data': 127, 'selected': 125}},
              {'id': 127, 'name': 'printer_data', 'view': ['client_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.printer_id']},
              {'id': 128, 'name': 'printer_display', 'view': ['client_edit'], 'update_func': [], 'update_req': [], 'data_request': 'requestLoadStore', 'db_pointer': ['printer_list.name']},

              {'id': 25, 'name': 'Subject', 'view': ['email_selector'], 'update_func': [None], 'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['email_select.name']},
              {'id': 124, 'name': 'To', 'view': ['email_recipient'], 'update_func': [], 'update_args': [], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['email_recipient.email'], 're': '[A-Za-z0-9]+'},
              {'id': 24, 'name': 'Contents', 'view': ['email_selector'], 'update_func': ['saveDataStore'], 'update_args': [None], 'update_req': [None],
               'data_request': 'requestLoadStore', 'db_pointer': ['email_select.text'], 're': '[A-Za-z0-9]+'},

              {'id': 130, 'name': 'order_id', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore',
               'db_pointer': ['retoucher_list.order_id'], 're': '[A-Za-z]+'},
              {'id': 135, 'name': 'status', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['retoucher_list.retoucher_status'], 're': '[A-Za-z]+'},
              {'id': 134, 'name': 'slug', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['retoucher_list.slug'], 're': '[A-Za-z]+'},
              {'id': 131, 'name': 'appointment date', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['retoucher_list.appointment_date'], 're': '[A-Za-z]+'},
              {'id': 132, 'name': 'address', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['retoucher_list.user_address'], 're': '[A-Za-z]+'},
              {'id': 133, 'name': 'photos', 'view': ['master_retoucher'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['retoucher_list.original_photos'], 're': '[A-Za-z]+'},


              {'id': 136, 'name': 'job_id', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore',
               'db_pointer': ['__printer_list.job_id'], 're': '[A-Za-z]+'},
              {'id': 137, 'name': 'status', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.printer_status'], 're': '[A-Za-z]+'},
              {'id': 143, 'name': 'slug', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_order_pin.slug'], 're': '[A-Za-z]+'},
              {'id': 144, 'name': 'first name', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_client_pin.fname'], 're': '[A-Za-z]+'},
              {'id': 145, 'name': 'last name', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_client_pin.lname'], 're': '[A-Za-z]+'},
              {'id': 138, 'name': 'qunatity', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.qty'], 're': '[A-Za-z]+'},
              {'id': 139, 'name': 'shipping', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.ship'], 're': '[A-Za-z]+'},
              {'id': 140, 'name': 'PDF', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.pdf'], 're': '[A-Za-z]+'},
              {'id': 162, 'name': 'link', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.link'], 're': '[A-Za-z]+'},
              {'id': 141, 'name': 'estimate', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.estimate'], 're': '[A-Za-z]+'},
              {'id': 142, 'name': 'shipping info', 'view': ['master_printer'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__printer_list.shipping_info'], 're': '[A-Za-z]+'},

              {'id': 158, 'name': 'estimate', 'view': ['templates'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__template_list.name'], 're': '[A-Za-z]+'},
              {'id': 159, 'name': 'shipping info', 'view': ['templates'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__template_list.direct_link'], 're': '[A-Za-z]+'},
              {'id': 160, 'name': 'shipping info', 'view': ['templates'], 'update_func': [None],
               'update_args': [None], 'update_req': [None], 'data_request': 'requestLoadStore', 'db_pointer': ['__template_list.prefix'], 're': '[A-Za-z]+'},
              ]


#Select batch automatically skips_reload if given args
control_list = [{'id': 1, 'name': 'orders', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'order'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_selector'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 3, 'name': 'clients', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'client'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client_selector'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 10, 'name': 'contractors', 'view': 'main_menu',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [88]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user_role_selector'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user_selector'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 9, 'name': 'brokerages', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'brokerages'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'brokerages_selector'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 2, 'name': 'settings', 'view': 'main_menu',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [7, 8]}},
                               {'controller': 'page', 'method': 'switchView', 'args': '_settings_packages'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 14, 'name': 'master retoucher', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'master_retoucher'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 15, 'name': 'master printer', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'master_printer'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},
                {'id': 16, 'name': 'templates', 'view': 'main_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'templates'},
                               {'controller': 'form', 'method': 'clearLock', 'args': None}]},

                {'id': 6, 'name': 'details', 'view': 'order_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'order_edit'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_client_list'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_selector'}]},
                {'id': 5, 'name': 'templates', 'view': 'order_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'order_templates'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_order_selector'}]},
                {'id': 7, 'name': 'invoice', 'view': 'order_edit_menu',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [122, 129, 45]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_invoice'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_invoice_additional'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_invoice_client'}]},
                {'id': 8, 'name': 'printer', 'view': 'order_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'order_printer'}]},
                {'id': 4, 'name': 'email', 'view': 'order_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'email_recipient'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'email_selector'}]},

                {'id': 11, 'name': 'templates', 'view': 'suborder_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'suborder_templates'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_suborder_selector'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_suborder_selector'}]},
                {'id': 12, 'name': 'invoice', 'view': 'suborder_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'suborder_invoice'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_invoice_additional'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_invoice_client'}]},
                {'id': 13, 'name': 'printer', 'view': 'suborder_edit_menu',
                 'on_submit': [{'controller': 'page', 'method': 'switchView', 'args': 'suborder_printer'}]},



                {'id': 'delete_batch', 'name': 'delete', 'view': '_settings_packages',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': []}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'order_client_list',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': [35]}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'order_templates',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': [37]}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'order_invoice_additional',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': [45, 46]}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'client_email',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': []}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'email_recipient',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': []}]},
                {'id': 'delete_batch', 'name': 'delete', 'view': 'suborder_templates',
                 'on_submit': [{'controller': 'form', 'method': 'deleteBatch', 'args': [102]}]},


                {'id': 'select_batch', 'name': 'select', 'view': 'client_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [12]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [10, 11, 16, 26, 125, 126, 7,8 ]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client_edit'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'order_client_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [12]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [0, 23, 34, 36]}},
                               {'controller': 'form', 'method': 'collapseDataStore', 'args': {'from': [12], 'to': [35]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_details'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_edit'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_client_list'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'order_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [20]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [1, 4, 27, 5, 9, 0, 23, 34, 35, 37,161, 38, 39, 42, 44, 45, 46, 49, 94, 95]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_details'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_edit'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_client_list'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_selector'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'template_order_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [40]},
                               {'controller': 'form', 'method': 'collapseDataStore', 'args': {'from': [40], 'to': [37]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'order_templates'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_order_selector'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'brokerages_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [57]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [58, 59, 60, 61, 62, 63, 64, 65]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'brokerages_details'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'user_role_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [88]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [73]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user_selector'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'user_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [73]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [79, 86, 87, 80, 81, 82, 83, 84, 85, 91, 88, 89, 93]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user_edit'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'suborder_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [94]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [96, 97, 98, 99, 100, 101, 102, 117,115,116]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_details'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_templates'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_suborder_selector'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_edit_menu'}]},
                {'id': 'select_batch', 'name': 'select', 'view': 'template_suborder_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': [100]},
                               {'controller': 'form', 'method': 'removeFields', 'args': {'fields': [96]}},
                               {'controller': 'form', 'method': 'collapseDataStore', 'args': {'from': [100], 'to': [102]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'suborder_templates'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'template_suborder_selector'}]},


                {'id': 'add_record', 'name': 'add', 'view': 'client',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [12, 10, 11, 15, 16, 26, 7,8 ]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client_edit'}]},
                {'id': 'add_record', 'name': 'add', 'view': 'order',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [1, 20, 0, 23, 34, 35, 4, 27, 5, 9, 37,161, 38, 39, 42, 44, 45, 46, 49, 147,148,149,150,151,152]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client_selector'},
                               {'controller': 'page', 'method': 'pinView', 'args': {'select_batch': 'order_client_selector'}}]},
                {'id': 'add_record', 'name': 'add', 'view': 'order_client_list',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [12, 13, 14, 15]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client'},
                               {'controller': 'page', 'method': 'switchView', 'args': 'client_selector'},
                               {'controller': 'page', 'method': 'pinView', 'args': {'select_batch': 'order_client_selector'}}]},
                {'id': 'add_record', 'name': 'add', 'view': 'brokerages',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [57, 58, 59, 60, 61, 62, 63, 64, 65]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'brokerages_details'}]},
                {'id': 'add_record', 'name': 'add', 'view': 'user',
                 'on_submit': [{'controller': 'form', 'method': 'removeFields', 'args': {'fields': [79, 86, 87, 80, 81, 82, 83, 84, 85, 91, 88, 89, 73, 93]}},
                               {'controller': 'page', 'method': 'switchView', 'args': 'user_edit'}]},

                {'id': 'send_email', 'name': 'select', 'view': 'email_selector',
                 'on_submit': [{'controller': 'form', 'method': 'selectBatch', 'args': []},
                               {'controller': 'comms', 'method': 'email', 'args': {'to': 124, 'subject': 25, 'content': 24}}]},

                {'id': 'upload', 'name': 'select', 'view': 'brokerages_details',
                 'on_submit': [{'controller': 'comms', 'method': 'upload', 'args': {'link': 27}}]},
                ]
#For deletes args are just additional fields to delete, don't add the ones already in the sControl args
#{'controller': 'form', 'method': 'mergeDataStore', 'args': {'from': [13,14], 'to': [10,11], 'format': [12]}},{'controller': 'page', 'method': 'switchView', 'args': 'client'},
#                               {'controller': 'form', 'method': 'updateDB', 'args': {'query_set_id': 'print_job_save', 'fields': [40]}},

initial_views = ['top_wrap', 'main_menu', 'order', 'order_selector']

#The check query result is used to select the appropriate query. The check query needs to be a count query (for now). The count will be used to select query from list.
#queryLoad will return a list of as many results as there are, or a single row (not list)
query_list = [  {'id': '_settings_packages_save', "check": "SELECT COUNT(id) FROM settings WHERE id = 'package_settings'",
                    "query": ["INSERT INTO settings (id, pickle) VALUES ('package_settings', {0['pickle']})", "UPDATE settings SET pickle = {0[pickle]} WHERE id = 'package_settings'"],
                    'function': 'queryStore'},
                {'id': '_settings_packages_load', 'query': "SELECT * FROM settings WHERE id = 'package_settings'", 'function': 'queryLoad'},

                {'id': 'client_find', 'query': "SELECT client_id FROM clients WHERE fname = '{0[10]}' AND lname = '{0[11]}'", 'function': 'queryLoad'},
                {'id': 'client_select', 'query': "SELECT client_id, fname, lname, phone FROM clients WHERE fname||lname||phone ILIKE '%{0[17]}%'", 'function': 'queryLoad'},
                {'id': 'client_load', 'query': "SELECT fname, lname, phone, email, printer_id FROM clients WHERE client_id = '{0[12]}'", 'function': 'queryLoad'},
                {'id': 'client_save', 'pointer': 12, 'query': ["INSERT INTO clients (fname, lname, phone, email, printer_id) VALUES (%(10)s, %(11)s, %(16)s, %(123)s, %(126)s) RETURNING client_id",
                    "UPDATE clients SET fname=%(10)s, lname=%(11)s, phone=%(16)s, email=%(123)s, printer_id=%(126)s WHERE client_id = %(12)s"], 'function': 'queryStoreBoolPointerNI'},
                {'id': 'client_package_save', "check": "SELECT COUNT(id) FROM client_prices WHERE client_id = '{0[12]}' AND package='{0[7]}'",
                 'query': ["INSERT INTO client_prices (client_id, package, price) VALUES ('{0[12]}', '{0[7]}', '{0[8]}')",
                    "UPDATE client_prices SET price = '{0[8]}' WHERE client_id = '{0[12]}' AND package = '{0[7]}'"], 'function': 'queryStore'},
                {'id': 'client_price_load', 'query': "SELECT price, package FROM client_prices WHERE client_id = '{0[12]}'", 'function': 'queryLoad'},


                {'id': 'order_client_find', 'query': "SELECT client_id, fname, lname FROM clients WHERE fname||lname||phone ILIKE '%{0[18]}%'", 'function': 'queryLoad'},
                {'id': 'order_client_select', 'query': "SELECT order_id, client_id, slug FROM orders WHERE '{0[order_client_find.client_id]}' = ANY (client_id) ORDER BY timestamp DESC", 'function': 'queryLoad'},
                {'id': 'order_slug_select', 'query': "SELECT order_id, client_id, slug FROM orders WHERE slug ILIKE '%{0[18]}%' ORDER BY timestamp DESC", 'function': 'queryLoad'},
                {'id': 'order_client_pin', 'query': "SELECT client_id, fname, lname FROM clients WHERE client_id = '{0[order_slug_select.client_id]}'", 'function': 'queryLoad'},
                {'id': 'order_client_pin_', 'query': "SELECT client_id, fname, lname FROM clients WHERE client_id = '{0[order_client_select.client_id]}'", 'function': 'queryLoad'},
                {'id': 'order_client_repin', 'query': "SELECT client_id, fname, lname, email FROM clients WHERE client_id = '{0[35]}'", 'function': 'queryLoad'},
                {'id': 'order_load', 'query': "SELECT client_id, slug, user_address, appointment_date, package, template_id, qty, bool_photos, bool_panorama, bool_map, bool_streetview, title, description,"
                                              "music, bool_music, template_links FROM orders WHERE order_id = '{0[20]}'",
                 'function': 'queryLoad'},
                {'id': 'order_save', 'pointer': 20, 'query': ["INSERT INTO orders (client_id, slug, user_address, appointment_date, package, unit, street_number, street, town, province, country, postal_code, "
                                                              "timestamp, bool_photos, bool_panorama, bool_map, bool_streetview, title, description, music) VALUES (%(36)s, %(4)s, %(1)s, %(27)s, %(9)s, "
                                                              "%(2)s, %(30)s, %(28)s, %(3)s, %(32)s, %(31)s, %(29)s, CURRENT_TIMESTAMP, %(147)s, %(148)s, %(149)s, %(150)s, "
                                                              "%(151)s, %(152)s, %(153)s) RETURNING order_id",
                    "UPDATE orders SET slug=%(4)s, client_id=%(36)s, user_address = %(1)s, appointment_date = %(27)s, package = %(9)s, bool_photos=%(147)s, bool_panorama=%(148)s, bool_map=%(149)s, title=%(151)s, "
                    "bool_streetview=%(150)s, description=%(152)s, music=%(153)s WHERE order_id = %(20)s RETURNING order_id"],
                 'function': 'queryStoreBoolPointerNI'},
                 {'id': 'order_save_address_comp', 'pointer': 20, 'query': [False,
                    "UPDATE orders SET unit={0[2]}, street_number='{0[30]}', street='{0[28]}', town='{0[3]}', province='{0[32]}', country='{0[31]}', postal_code='{0[29]}' WHERE order_id = '{0[20]}'"], 'function': 'queryStoreBoolPointer'},
                {'id': 'order_client_printer', 'query': "SELECT printer_id FROM clients WHERE client_id = '{0[35]}'", 'function': 'queryLoad'},
                {'id': 'order_client_price', 'query': "SELECT price FROM client_prices WHERE client_id='{0[35]}' AND package='{0[9]}'", 'function': 'queryLoad'},

                {'id': 'template_list', 'query': "SELECT id, name FROM bs_templates", 'function': 'queryLoad'},
                {'id': 'template_pin', 'query': "SELECT name FROM bs_templates WHERE id='{0[37]}'", 'function': 'queryLoad'},
                {'id': 'template_link_pin', 'query': "SELECT template_links FROM orders WHERE order_id='{0[20]}'", 'function': 'queryLoad'},
                {'id': 'template_save', 'pointer': 20, 'query': [False, "UPDATE orders SET template_id=ARRAY{0[39]} WHERE order_id = '{0[20]}'"], 'function': 'queryStoreBoolPointer'},
                {'id': 'print_job_save', "check": "SELECT COUNT(job_id) FROM print_jobs WHERE order_id = '{0[20]}' AND template_id = '{0[37]}' AND suborder_id = NULL",
                    "query": ["INSERT INTO print_jobs (template_id, order_id, printer_status) VALUES ('{0[37]}', '{0[20]}', FALSE)", False], 'function': 'queryStore'},
                {'id': 'print_job_delete', 'pointer': 20, 'query': [False, "DELETE FROM print_jobs WHERE order_id='{0[20]}' AND suborder_id = NULL AND template_id <> ALL (ARRAY{0[39]})"], 'function': 'queryStoreBoolPointer'},
                {'id': 'print_job_update_qty', "check": "SELECT COUNT(job_id) FROM print_jobs WHERE order_id = '{0[20]}' AND template_id = '{0[129]}'", 'field_analyze': '{0[129]}{0[45]}{0[20]}',
                    "query": [False, "UPDATE print_jobs SET qty = '{0[45]}' WHERE order_id = '{0[20]}' AND template_id = '{0[129]}' AND suborder_id=NULL"], 'function': 'queryStore'},

                {'id': 'subprint_job_save', "check": "SELECT COUNT(job_id) FROM print_jobs WHERE order_id = '{0[20]}' AND template_id = '{0[102]}' AND suborder_id={0[94]}",
                    "query": ["INSERT INTO print_jobs (template_id, order_id, qty, suborder_id, printer_status) VALUES ('{0[102]}', '{0[20]}', '{0[98]}', '{0[94]}', FALSE)",
                              "UPDATE print_jobs SET qty = '{0[98]}' WHERE order_id = '{0[20]}' AND template_id = '{0[102]}' AND suborder_id='{0[94]}'"], 'function': 'queryStore'},
                {'id': 'subprint_job_delete', 'pointer': 94, 'query': [False, "DELETE FROM print_jobs WHERE order_id='{0[20]}' AND suborder_id ='{0[94]}' AND template_id <> ALL (ARRAY{0[97]})"], 'function': 'queryStoreBoolPointer'},
                {'id': 'subprint_update_qty', 'pointer': 94, 'query': [False, "UPDATE print_jobs SET qty='{0[98]}', price='{0[107]}' WHERE suborder_id = '{0[94]}' AND order_id='{0[20]}' AND template_id='{0[146]}'"], 'function': 'queryStoreBoolPointer'},
                {'id': 'subprint_update_qty_', 'pointer': 94, 'query': [False, "UPDATE print_jobs SET qty='{0[105]}', price='{0[107]}' WHERE suborder_id = '{0[94]}' AND order_id='{0[20]}' AND template_id='{0[146]}'"],
                 'function': 'queryStoreBoolPointer'},
                {'id': 'print_update_printer', 'pointer': 20, 'query': [False, "UPDATE print_jobs SET ship='{0[119]}' WHERE suborder_id IS NULL AND order_id='{0[20]}'"],
                 'function': 'queryStoreBoolPointer'},

                {'id': 'invoice_load', 'query': "SELECT price, shipping, qty, split, template_id FROM orders WHERE order_id='{0[20]}'", 'function': 'queryLoad'},
                {'id': 'invoice_save', 'pointer': 20, 'query': [False, "UPDATE orders SET package='{0[9]}', price='{0[42]}', shipping='{0[44]}', qty=ARRAY{0[47]}, split=ARRAY{0[50]} WHERE order_id = '{0[20]}'"], 'function': 'queryStoreBoolPointer'},
                {'id': 'template_pin_', 'query': "SELECT name FROM templates WHERE template_id='{0[129]}'", 'function': 'queryLoad'},

                {'id': 'printer_list', 'query': "SELECT printer_id, name FROM printers", 'function': 'queryLoad'},
                {'id': 'printer_load', 'query': "SELECT ship, shipping_address FROM orders WHERE order_id = '{0[20]}'", 'function': 'queryLoad'},
                {'id': 'printer_save', 'pointer': 20, 'query': [False,
                    "UPDATE orders SET printer_id=%(52)s, ship=%(119)s, shipping_address = %(120)s WHERE order_id = %(20)s"], 'function': 'queryStoreBoolPointerNI'},


                {'id': 'suborder_list', 'query': "SELECT suborder_id, date FROM suborders WHERE parent_id = '{0[20]}'", 'function': 'queryLoad'},
                {'id': 'suborder_load', 'query': "SELECT template_id, printer_id FROM suborders WHERE suborder_id = '{0[94]}'", 'function': 'queryLoad'},
                {'id': 'template_load', 'query': "SELECT qty FROM suborders WHERE suborder_id = '{0[94]}'", 'function': 'queryLoad'},

                {'id': 'sub_template_pin', 'query': "SELECT name FROM templates WHERE template_id='{0[102]}'", 'function': 'queryLoad'},
                {'id': 'suborder_template_save', 'pointer': 94, 'query': [False, "UPDATE suborders SET template_id=ARRAY{0[97]}, qty=ARRAY{0[99]} WHERE suborder_id = '{0[94]}'"], 'function': 'queryStoreBoolPointer'},

                {'id': 'subinvoice_load', 'query': "SELECT shipping, qty, split, template_id FROM suborders WHERE suborder_id='{0[94]}'", 'follow_key': 'template_id', 'function': 'queryLoad'},
                {'id': 'subinvoice_subtotal_load', 'query': "SELECT price FROM print_jobs WHERE suborder_id='{0[94]}' AND order_id = '{0[20]}'", 'function': 'queryLoad'},
                {'id': 'sub_template_pin_', 'query': "SELECT name FROM templates WHERE template_id='{0[subinvoice_load.template_id]}'", 'function': 'queryLoad'},
                {'id': 'subinvoice_save', 'pointer': 94, 'query': [False, "UPDATE suborders SET qty=ARRAY{0[106]}, shipping='{0[110]}', split=ARRAY{0[104]} WHERE suborder_id = '{0[94]}'"],
                 'function': 'queryStoreBoolPointer'},

                {'id': 'subprinter_load', 'query': "SELECT ship, shipping_address FROM suborders WHERE suborder_id = '{0[94]}'", 'function': 'queryLoad'},
                {'id': 'subprinter_save', 'pointer': 94, 'query': [False, "UPDATE suborders SET ship='{0[116]}', printer_id='{0[112]}', shipping_address='{0[115]}' WHERE suborder_id = '{0[94]}'"],
                 'function': 'queryStoreBoolPointer'},
                {'id': 'subprint_update_printer', 'pointer': 94, 'query': [False, "UPDATE print_jobs SET ship='{0[116]}' WHERE suborder_id = '{0[94]}' AND order_id='{0[20]}'"],
                 'function': 'queryStoreBoolPointer'},

                {'id': 'brokerages_list', 'query': "SELECT brokerage_id, name, address, phone, fax, notes, site, logo FROM brokerages WHERE name||address||phone||fax||site||notes ILIKE '%{0[55]}%'", 'function': 'queryLoad'},
                {'id': 'brokerages_pin', 'query': "SELECT name, address, phone, fax, notes, site, logo FROM brokerages WHERE brokerage_id='{0[57]}'", 'function': 'queryLoad'},
                {'id': 'brokerages_save', 'pointer': 57, 'query': ["INSERT INTO brokerages (name, address, phone, fax, site, notes, logo) VALUES (%(58)s, %(59)s, %(60)s, %(61)s, %(62)s, %(65)s, %(64)s) RETURNING brokerage_id",
                    "UPDATE brokerages SET name=%(58)s, address=%(59)s, phone = %(60)s, fax = %(61)s, site = %(62)s, notes=%(65)s, logo=%(64)s WHERE brokerage_id = %(57)s RETURNING brokerage_id"], 'function': 'queryStoreBoolPointerNI'},

                {'id': 'user_list', 'query': "SELECT user_id, user_name, name, address, email, phone, role FROM users WHERE (user_name||name||address||phone||email||notes ILIKE '%{0[72]}%' AND role = '{0[88]}')", 'function': 'queryLoad'},
                {'id': 'user_load', 'query': "SELECT user_name, name, address, email, phone, role, notes, password, status, site, skype FROM users WHERE user_id = '{0[73]}'", 'function': 'queryLoad'},
                {'id': 'user_role_list', 'query': "SELECT role_id, name FROM user_roles", 'function': 'queryLoad'},
                {'id': '_user_role_list', 'query': "SELECT name FROM user_roles WHERE role_id = '{0[user_list.role]}'", 'function': 'queryLoad'},
                {'id': 'user_save', 'pointer': 73, 'query': ["INSERT INTO users (name, user_name, password, role, phone, email, site, skype, notes, status, address) VALUES (%(79)s, %(86)s, %(87)s, %(80)s, %(81)s, %(82)s, %(83)s, %(84)s, %(85)s, %(91)s, %(92)s) RETURNING user_id",
                    "UPDATE users SET name=%(79)s, user_name=%(86)s, password=%(87)s, role=%(80)s, phone = %(81)s, email = %(82)s, site = %(83)s, skype=%(84)s, notes=%(85)s, status=%(91)s, address=%(92)s WHERE user_id = %(73)s RETURNING user_id"], 'function': 'queryStoreBoolPointerNI'},

                {'id': 'email_select', 'query': "SELECT text, name FROM emails", 'function': 'queryLoad'},
                {'id': 'email_recipient', 'query': "SELECT email FROM clients WHERE client_id = '{0[35]}'", 'function': 'queryLoad'},


                {'id': 'retoucher_list', 'query': "SELECT order_id, slug, user_address, appointment_date, original_photos, retoucher_status FROM orders", 'function': 'queryLoad'},
                {'id': '__printer_list', 'query': "SELECT job_id, order_id, ship, pdf, qty, printer_status, estimate, shipping_info, link FROM print_jobs", 'function': 'queryLoad'},
                {'id': '__printer_order_pin', 'query': "SELECT slug, client_id FROM orders WHERE order_id = '{0[__printer_list.order_id]}'", 'function': 'queryLoad'},
                {'id': '__printer_client_pin', 'query': "SELECT fname, lname FROM clients WHERE client_id = '{0[__printer_order_pin.client_id]}'", 'function': 'queryLoad'},

                {'id': '__template_list', 'query': "SELECT name, direct_link FROM bs_templates", 'function': 'queryLoad'},

                ]

#{'id': 'brokerages_save', 'pointer': 57, 'query': ["INSERT INTO brokerages (name, address, phone, fax, site, notes, logo) VALUES ('{0[58]}', '{0[59]}', '{0[60]}', '{0[61]}', {0[62]}, '{0[65]}', '{0[64]}') RETURNING brokerage_id",
#                    "UPDATE brokerages SET name='{0[58]}', address='{0[59]}', phone = '{0[60]}', fax = '{0[61]}', site = '{0[62]}', notes='{0[65]}', logo='{0[64]}' WHERE brokerage_id = '{0[57]}' brokerage_id"], 'function': 'queryStoreBoolPointerNI'},

#To specify a field from a previous request type "query_id.field_name"

#Fields for which data should always be reloaded
#'order_client_find.client_id', 'order_client_find.fname', 'order_client_find.lname'
query_set_list = [  {'id': 'client_find', 'set': ['client_find'], 'final_format': ['client_find.client_id']},
                    {'id': 'client_save', 'set': ['client_save'], 'final_format': ['client_save.client_id']},
                    {'id': 'client_select', 'set': ['client_select'], 'final_format': ['client_select.client_id', 'client_select.fname', 'client_select.lname', 'client_select.phone']},
                    {'id': 'client_load', 'set': ['client_load'], 'final_format': ['client_load.fname', 'client_load.lname', 'client_load.phone',
                                                                                   'client_load.email', 'client_load.printer_id']},
                    {'id': 'client_package_save', 'set': ['client_package_save']},
                    {'id': 'client_price_load', 'set': ['client_price_load'], 'final_format': ['client_price_load.price', 'client_price_load.package']},


                    {'id': 'order_select_byclient', 'set': ['order_client_find', 'order_client_select', 'order_client_pin_'],
                     'final_format': ['order_client_select.order_id', 'order_client_select.slug', 'order_client_pin_.fname', 'order_client_pin_.lname']},
                    {'id': 'order_select_byslug', 'set': ['order_slug_select', 'order_client_pin'],
                     'final_format': ['order_slug_select.order_id', 'order_slug_select.slug', 'order_client_pin.fname', 'order_client_pin.lname']},
                    {'id': 'order_load', 'set': ['order_load'], 'final_format':
                        ['order_load.slug', 'order_load.client_id', 'order_load.user_address', 'order_load.appointment_date', 'order_load.package', 'order_load.template_id', 'order_load.qty', 'order_load.bool_photos',
                         'order_load.bool_panorama', 'order_load.bool_map', 'order_load.bool_streetview', 'order_load.title', 'order_load.description', 'order_load.music',
                         'order_load.bool_music', 'order_load.template_links']},
                    {'id': 'order_load_clients_data', 'set': ['order_client_repin'], 'final_format': ['order_client_repin.fname', 'order_client_repin.lname', 'order_client_repin.email']},
                    {'id': 'order_load_clients', 'set': ['order_client_repin'], 'final_format': ['order_client_repin.client_id', 'order_client_repin.fname', 'order_client_repin.lname', 'order_client_repin.email']},
                    {'id': 'order_save', 'set': ['order_save'], 'final_format': ['order_save.order_id']},
                    {'id': 'order_save_address_comp', 'set': ['order_save_address_comp']},
                    {'id': 'order_client_printer', 'set': ['order_client_printer'], 'final_format': ['order_client_printer.printer_id']},
                    {'id': 'order_client_price', 'set': ['order_client_price'], 'final_format': ['order_client_price.price']},

                    {'id': 'template_list', 'set': ['template_list'], 'final_format': ['template_list.id', 'template_list.name']},
                    {'id': 'template_pin', 'set': ['template_pin'], 'final_format': ['template_pin.name']},
                    {'id': 'template_link_pin', 'set': ['template_link_pin'], 'final_format': ['template_link_pin.template_links']},
                    {'id': 'template_save', 'set': ['template_save']},
                    {'id': 'print_job_save', 'set': ['print_job_save']},
                    {'id': 'print_job_delete', 'set': ['print_job_delete']},
                    {'id': 'print_job_update_qty', 'set': ['print_job_update_qty']},
                    {'id': 'print_job_update_ship', 'set': ['print_job_update_ship']},
                    {'id': 'print_update_printer', 'set': ['print_update_printer']},


                    {'id': 'printer_list', 'set': ['printer_list'], 'final_format': ['printer_list.printer_id', 'printer_list.name']},
                    {'id': 'printer_load', 'set': ['printer_load'], 'final_format': ['printer_load.ship', 'printer_load.shipping_address']},
                    {'id': 'printer_save', 'set': ['printer_save']},

                    {'id': 'invoice_load', 'set': ['invoice_load'], 'final_format': ['invoice_load.price', 'invoice_load.shipping', 'invoice_load.qty', 'invoice_load.split', 'invoice_load.template_id']},
                    {'id': 'template_pin_', 'set': ['template_pin_'], 'final_format': ['template_pin_.name']},
                    {'id': 'invoice_save', 'set': ['invoice_save']},

                    {'id': 'suborder_list', 'set': ['suborder_list'], 'final_format': ['suborder_list.suborder_id', 'suborder_list.date']},
                    {'id': 'suborder_load', 'set': ['suborder_load'], 'final_format': ['suborder_load.template_id', 'suborder_load.printer_id']},
                    {'id': 'template_load', 'set': ['template_load'], 'final_format': ['template_load.qty']},
                    {'id': 'sub_template_pin', 'set': ['sub_template_pin'], 'final_format': ['sub_template_pin.name']},

                    {'id': 'subinvoice_load', 'set': ['subinvoice_load', 'sub_template_pin_'], 'final_format': ['subinvoice_load.qty', 'subinvoice_load.shipping', 'subinvoice_load.template_id', 'sub_template_pin_.name', 'subinvoice_load.split']},
                    {'id': 'subinvoice_subtotal_load', 'set': ['subinvoice_subtotal_load'], 'final_format': ['subinvoice_subtotal_load.price']},
                    {'id': 'subinvoice_save', 'set': ['subinvoice_save']},
                    {'id': 'subprint_update_qty', 'set': ['subprint_update_qty']},
                    {'id': 'subprint_update_qty_', 'set': ['subprint_update_qty_']},

                    {'id': 'suborder_template_save', 'set': ['suborder_template_save']},
                    {'id': 'subprint_job_save', 'set': ['subprint_job_save']},
                    {'id': 'subprint_job_delete', 'set': ['subprint_job_delete']},

                    {'id': 'subprinter_load', 'set': ['subprinter_load'], 'final_format': ['subprinter_load.ship', 'subprinter_load.shipping_address']},
                    {'id': 'subprinter_save', 'set': ['subprinter_save']},
                    {'id': 'subprint_update_printer', 'set': ['subprint_update_printer']},

                    {'id': 'brokerages_list', 'set': ['brokerages_list'], 'final_format': ['brokerages_list.brokerage_id', 'brokerages_list.name', 'brokerages_list.address', 'brokerages_list.phone', 'brokerages_list.fax', 'brokerages_list.site', 'brokerages_list.notes', 'brokerages_list.logo']},
                    {'id': 'brokerages_pin', 'set': ['brokerages_pin'],
                     'final_format': ['brokerages_pin.name', 'brokerages_pin.address', 'brokerages_pin.phone', 'brokerages_pin.fax', 'brokerages_pin.site', 'brokerages_pin.notes', 'brokerages_pin.logo']},
                    {'id': 'brokerages_save', 'set': ['brokerages_save'], 'final_format': ['brokerages_save.brokerage_id']},


                    {'id': 'user_list', 'set': ['user_list', '_user_role_list'], 'final_format': ['user_list.user_id', 'user_list.user_name', 'user_list.name', 'user_list.address', 'user_list.phone', 'user_list.email', '_user_role_list.name']},
                    {'id': 'user_load', 'set': ['user_load'], 'final_format': ['user_load.user_name', 'user_load.name', 'user_load.address', 'user_load.phone', 'user_load.email', 'user_load.role',
                                                                               'user_load.password', 'user_load.notes', 'user_load.site', 'user_load.skype', 'user_load.status']},
                    {'id': 'user_role_list', 'set': ['user_role_list'], 'final_format': ['user_role_list.role_id', 'user_role_list.name']},
                    {'id': 'user_save', 'set': ['user_save'], 'final_format': ['user_save.user_id']},


                    {'id': '_settings_packages_save', 'set': ['_settings_packages_save']},
                    {'id': '_settings_packages_load', 'set': ['_settings_packages_load'], 'final_format': ['_settings_packages_load.pickle']},

                    {'id': 'email_select', 'set': ['email_select'], 'final_format': ['email_select.text', 'email_select.name']},
                    {'id': 'email_recipient', 'set': ['email_recipient'], 'final_format': ['email_recipient.email']},


                    {'id': 'retoucher_list', 'set': ['retoucher_list'], 'final_format':
                        ['retoucher_list.slug', 'retoucher_list.user_address', 'retoucher_list.appointment_date', 'retoucher_list.original_photos', 'retoucher_list.order_id', 'retoucher_list.retoucher_status']},
                    {'id': '__printer_list', 'set': ['__printer_list', '__printer_order_pin', '__printer_client_pin'], 'final_format':
                        ['__printer_list.job_id', '__printer_list.order_id', '__printer_list.ship', '__printer_list.pdf', '__printer_list.qty', '__printer_list.printer_status',
                         '__printer_list.estimate', '__printer_list.shipping_info', '__printer_list.link', '__printer_order_pin.slug', '__printer_client_pin.fname', '__printer_client_pin.lname']},

                    {'id': '__template_list', 'set': ['__template_list'], 'final_format':
                        ['__template_list.name', '__template_list.direct_link']},
                    ]

#Remove 12? For new order for example. 10, 11, 16, 12?
#13, 14, 15,
fields_reload = [19, 21, 22, 24,25,124, 38,39, 96,97,99,98, 123, 111,146,105,107]
#Fields which should not be sent to the client side
fields_hidden = [12, 20, 9, 35, 36, 37, 39, 47, 48, 51,53,54, 57, 73, 88, 93, 94, 102,97,99,100, 146,104,106,108, 117,113,114, 123, 125,127,128, 129, 130, 136, 153,155,156]
#Cannot be empty. Don't send if are.
fields_nonempty = [37, 38]
#Do not apply batch_base to these fields.
fields_non_base = [97, 99]
#For fields (mainly defaults) that need to only be put in once, but end up getting repeated
fields_single = [51]

#37 - list of templates in an order record
#38 - reload because the names need to get updated. 37 stays intact (ids)
#39 - collapsed fields. clear.
#47 - collapsed additional qty.
#51, 53, 54 - printer option fields

#73 - user_id user list
#88 - user_role_selector role_id
#93 - selected for position in user edit
#94 - suborder_id

#102 - sub_templates in a suborder
#97 -
#99 - collapsed fields
#100 - templates id in a selector list

#117, 113, 114 - options fields for subprinter printer dropdown list

#123 - email_collapsed in client view

#24, 25 - email subject and contents
#124 - email recipients

#125, 126 - default pinter for clients
#129 - template id for order invoice
#130 - order_id for retoucher view
#136 - job_id print jobs

box_queries = {'retrieve': "SELECT pickle FROM settings WHERE id='box'", 'save': "UPDATE settings SET pickle={0} WHERE id='box'"}