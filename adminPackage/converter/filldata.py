from gevent import monkey; monkey.patch_all()
import cPickle
import psycopg2.extensions
from .. import settings, settings_local
import requests
import re
import base64
from string import Formatter
from .. import config
from ..converter import basic
import ast
import copy
import os
import shutil
from jinja2 import Environment, FileSystemLoader
import logging
#Filldata is not the best name really. But right now it doesn't matter. IN for when the form is loading, OUT is when it's getting submitted or updated


class dataManagementBase():

    def requestDB(self, args):
        data = self.datastoreToQuery(args['query_set_id'])
        result = self.pg.setDispatch(args['query_set_id'], data)
        ##print data, result, args
        if result:
            local_store = {}
            for _result in result:
                for key, value in _result.iteritems():
                    if not key in local_store:
                        local_store[key] = []
                    local_store[key].append(value)
                    #print 'requestDB'
            for key in local_store.keys():
                #print key, local_store[key]
                self.session['load_store'].append({'id': key, 'data_request': local_store[key]})
        #print 'requestDB load_store:', self.session['load_store']

    def datastoreToQuery(self, set_id=None, batch_id=None, fields=None):
        required_len = None

        if set_id:
            set = filter(lambda set: set['id'] == set_id, config.query_set_list)[0]
            query_list = set['set']

            required_fields = []
            for query_id in query_list:
                query_obj = filter(lambda query: query['id'] == query_id, config.query_list)[0]

                if not 'field_analyze' in query_obj:
                    query_string = query_obj['query']
                    # For situations like with queryStore, where there are multiple queries
                    if isinstance(query_string, list):
                        if query_string[0]:
                            query_string = query_string[0]
                        else:
                            query_string = query_string[1]
                else:
                    query_string = query_obj['field_analyze']

                analyzed = self.formatter.parse(query_string)
                for insert in analyzed:
                    insert = insert[1]
                    if insert:
                        split_insert = re.search('\[([0-9]+)\]', insert)
                        if split_insert:
                            required_fields.append(int(split_insert.group(1)))

                fields = filter(lambda field: field['id'] in required_fields, self.session['data_store'])
                if len(fields) > 0:
                    data_store = basic.splitBatches(fields)
                    required_len = len(required_fields)
                else:
                    return []
        else:
            if batch_id and fields:
                # Fields are for selecting only those within one batch. Works alike select_batch.
                data_store = filter(lambda _f: (_f['batch_id'] == batch_id and _f['id'] in fields) or not _f['id'] in fields, self.session['data_store'])
                data_store = [data_store]
            else:
                data_store = [self.session['data_store']]

        print 'dataStoreTQ', self.session['endpoints']['form'].view_config['id'], data_store
        final = []
        shift_field = []

        flag_skip = False
        if len(data_store) == 1:
            flag_skip = True


        # Preparing the list of shift fields and then making a copy, so that elements could be deleted.
        shift_ds_tq = []
        if 'shift_dsTQ' in self.session['endpoints']['form'].view_config:
            # If not list, then make into a list
            if not isinstance(self.session['endpoints']['form'].view_config['shift_dsTQ'], list):
                print 'not list'
                self.session['endpoints']['form'].view_config['shift_dsTQ'] = [self.session['endpoints']['form'].view_config['shift_dsTQ']]
            shift_ds_tq = list(self.session['endpoints']['form'].view_config['shift_dsTQ'])

        for _data_store in data_store:
            data = dict()
            if shift_field:
                _data_store += shift_field
                #print shift_field
            print _data_store
            for field in _data_store:
                #print field, self.session['endpoints']['form'].view_config['id'], ('shift_dsTQ' in self.session['endpoints']['form'].view_config)
                #Sometimes a field must be joined and repeated for other batches. Ex. {72: ''}, {88: 1}, {88: 2}  -> {72, 88}, {72, 88}
                #if 'shift_dsTQ' in self.session['endpoints']['form'].view_config and not shift_field:
                if field['id'] in shift_ds_tq:
                    shift_ds_tq.remove(field['id'])
                    shift_field.append(field)
                    # If there is only one element, skips to the next _data_store
                    if len(_data_store) == 1 and not flag_skip:
                            continue
                if 'to_db' in field:
                    data[field['id']] = field['to_db']
                else:
                    if not isinstance(field['data_request'], dict):
                        data[field['id']] = field['data_request']
                    elif 'to_db' in field['data_request']:
                        data[field['id']] = field['data_request']['to_db']
            if len(data.items()) != 0:
                if required_len:
                    if len(data.values()) == required_len:
                        final.append(data)
                else:
                    final.append(data)
        print '\t', final
        return final


class fillDataIn(dataManagementBase):
    #config fields are just all the fields from the config file
    def __init__(self, config_fields, session):
        self.config_fields = config_fields
        self.session = session
        self.pg = session['pg_storage']
        self.formatter = Formatter()

    def pickle(self, args):
        result = self.pg.setDispatch(args['query_set_id'])[0].values()
        if result[0]:
            upickle = cPickle.loads(result[0])
            return upickle

    def requestLoadStore(self, field_id, delete=True):  # Returns a list on success. All of field datafill functions must return a list with a 'data_request' dict
        load_store = self.session['load_store']
        field = filter(lambda field: field['id'] == field_id, self.config_fields)[0]

        flag_native = False  # If the loaded data already formatted as fields. For example pickle
        if 'db_pointer' in field:
            value = filter(lambda data: data['id'] in field['db_pointer'], load_store)
            #print 'db_pointer:', field['db_pointer'], value
            if value:
                value = value
            else:
                value = False
        else:
            flag_native = True
            value = filter(lambda data: data['id'] == field_id, load_store)
            if len(value) == 0:
                value = False

        #print 'requestLS'
        #print field_id, value
        remove_list = []
        if value:
            if flag_native:
                for _v in value:
                    remove_list.append(_v)
                    yield _v['data_request']
            else:
                for pointer_data in value:
                    remove_list.append(pointer_data)
                    for _v in pointer_data['data_request']:
                        if 'convert' in field:
                            yield str(_v)
                        else:
                            yield _v
            print 'load_store clean up', remove_list
            if delete:
                for rm in remove_list:
                    self.session['load_store'].remove(rm)
            print self.session['load_store']

        else:
            yield ''

    def mergeLoadStore(self, args):  # Removes duplicates
        print 'mergeLoadStore', self.session['load_store']
        target_id = args['target'][0]
        control_id = args['control']
        compare_id = args['compare']

        target_field = target_id+'.'+compare_id
        control_field = control_id+'.'+compare_id
        try:
            target = filter(lambda load_store: load_store['id'] == target_field, self.session['load_store'])[0]['data_request']
            control = filter(lambda load_store: load_store['id'] == control_field, self.session['load_store'])[0]['data_request']
        except:
            return

        remove_list = []
        print control
        print target
        for i, _target in enumerate(target):
            if _target in control:
                remove_list.append(i)
        print remove_list
        for target_id in args['target']:
            #print target_id, self.session['load_store']
            family = filter(lambda load_store: re.match(target_id, str(load_store['id'])), self.session['load_store'])
            #print family
            for member in family:
                print 'member', member
                for index in remove_list[::-1]:
                    del member['data_request'][index]

        #print 'mergeLoadStore:', self.session['load_store']

    def requestOptions(self, field_id):
        args = filter(lambda field: field['id'] == field_id, self.config_fields)[0]['data_args']
        data = []
        display = []
        to_db_index = 0
        to_db = False
        #Selected should be loaded from either load_store or data_store
        selected_field = filter(lambda field: field['id'] == args['selected'], self.config_fields)[0]
        selected = filter(lambda field: field['id'] == args['selected'], self.session['data_store'])  # Looking for pointer in data store
        if not selected:  # If not in data_store -> load_store
            pointer = selected_field['db_pointer']
            #Now to look for fields value in load store in case it is not already loaded
            #selected = filter(lambda field: field['id'] == pointer, self.session['load_store'])
            selected = [x for x in self.requestLoadStore(pointer)][0]
            if selected:
                selected = selected[0]['data_request']
                if selected is list():
                    selected = selected[0]
        else:
            _selected = selected[0]
            selected = selected[0]['data_request']

        # These need to field ids because requestLoadStore needs those.
        for _d in self.requestLoadStore(args['display'], delete=False):
            display.append(_d)
        for _d in self.requestLoadStore(args['data'], delete=False):
            data.append(_d)

        self.session['log'].logStore(selected)
        if not selected in display:
            # In case selected is referring to data rather than display. Problematic if both display and data are containing same elements
            if selected in data:
                to_db = selected
                index = data.index(selected)
                selected = display[index]
            else:
                selected = False

        if not selected:
            if _selected:
                _selected['data_request'] = display[0]
            selected = display[0]

        #Putting the default choice into data. Package -> price
        if selected and not to_db:
            to_db_index = display.index(selected)
            to_db = data[to_db_index]

        self.session['log'].logStore(display)
        self.session['log'].logStore(selected)
        self.session['log'].log('requestOptions')

        logging.debug('requestOptions %s\t %s', field_id, {'selected': selected, 'options': display, 'data': data, 'to_db': to_db})
        return [{'selected': selected, 'options': display, 'data': data, 'to_db': to_db}]

    def addressSplit(self, args):
        address = filter(lambda field: field['id'] == args['address'], self.session['data_store'])[0]['data_request']
        request_url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=' + address
        data = requests.get(request_url).json()

        print 'addressSplit:', address, data['status']

        if data['status'] == 'OK':
            components = data['results'][0]['address_components']
            geometry = data['results'][0]['geometry']
            for component in settings.geocode:
                value = filter(lambda _component: component in _component['types'], components)
                if value:
                    value = value[0]['long_name']
                    self.session['load_store'].append({'id': component, 'data_request': [value]})

            center = str(geometry['location']['lat']) + ',' + str(geometry['location']['lng'])
            northeast = str(geometry['viewport']['northeast']['lat']) + ',' + str(geometry['viewport']['northeast']['lng'])
            southwest = str(geometry['viewport']['southwest']['lat']) + ',' + str(geometry['viewport']['southwest']['lng'])

            map_url = 'http://maps.googleapis.com/maps/api/staticmap?center={center}&size={size}&sensor=false&key={key}&visible={northeast}|{southwest}&markers=color:red|{center}&.png'
            map_url = map_url.format(center=center, size=settings.order_map_size, key=settings.google_api_key, northeast=northeast, southwest=southwest)

            image = requests.get(map_url).content
            encoded_image = 'data:image/gif;base64,' + base64.b64encode(image)

            self.session['load_store'].append({'id': 'map_url', 'data_request': [encoded_image]})

    def separateData(self, args):  # Reformats the data so that a list from DB is separated into multiple fields
        pointers = filter(lambda pointer: pointer['id'] in args['pointers'], self.session['load_store'])
        print 'SeparateData'
        print self.session['load_store']
        print pointers
        for pointer in pointers:
            temp = []
            for value in pointer['data_request']:
                if value:
                    temp += value
            pointer['data_request'] = temp

    def pOrder(self, field_id):
        return [False]

    def _switchView(self, args):  # Checks for presence in data_store (Stops), and load_store (Continues)
        if 'check' in args:
            stop_fields = filter(lambda field: field['id'] in args['stop'], self.session['data_store'])
            check_fields = filter(lambda field: field['id'] in args['check'], self.session['load_store'])
            self.session['log'].logStore([stop_fields, check_fields], '_switchView check')
            if len(check_fields) == 0:
                return False
            for _f in stop_fields:
                if _f['data_request']:
                    return False
        temp_view_id = self.session['endpoints']['form'].view_config
        #print args['view_id']
        #self.session['endpoints']['form'].formSwitch(view_id=args['view_id'])
        self.session['controllers']['page'].switchView(args['view_id'])
        #self.session['endpoints']['form'].formSwitch(view_id=temp_view_id)

        return False  # so that no data is added to load store

    def encode(self, args):
        #Args - list of load_store pointers
        #function encodes binary (or other data) to base64 (expansion possible)
        for pointer in args:
            value = filter(lambda data: data['id'] == pointer, self.session['load_store'])[0]
            print value
            new_value = base64.b64encode(value['data_request'][0])
            value['data_request'][0] = new_value

    def replacePickle(self, args):
        # Takes comparison fields. Finds indcies where they correspond. Replaces corresponding indexes.
        load_store = self.session['load_store']
        compare = []
        replace = []

        _compare = filter(lambda _f: _f['id'] == args['compare'][0], load_store)
        compare.append(_compare)
        # The data loaded through db is in a different format than pickled data
        _compare = filter(lambda _f: _f['id'] == args['compare'][1], load_store)
        if not _compare:
            return
        else:
            _compare = _compare[0]['data_request']
        compare.append(_compare)

        _replace = filter(lambda _f: _f['id'] == args['replace'][0], load_store)
        replace.append(_replace)
        _replace = filter(lambda _f: _f['id'] == args['replace'][1], load_store)[0]
        replace.append(_replace)

        print 'replacePickle'
        indexes_replace = []
        for i, cp in enumerate(compare[0]):
            for z, cr in enumerate(compare[1]):
                if cp['data_request'] == cr:
                    indexes_replace.append([i, z])
        print replace[1]
        for r in indexes_replace:
            new = replace[1]['data_request'][r[1]]
            replace[0][r[0]]['data_request'] = new

    def default(self, args):
        #Adds fields into load_store if they are not present
        for id in args['fields']:
            field = filter(lambda field: field['id'] == id, self.session['load_store'])
            if not field:
                self.session['load_store'].append({'id': id, 'data_request': args['default']})

    def listMusic(self, args):
        # Uses the box function to append music data to load_store
        box = self.session['controllers']['box']
        self.session['load_store'].append({'id': args['name_pointer'], 'data_request': ['No Music']})  # Need to wrap in [] because of some quirks
        self.session['load_store'].append({'id': args['data_pointer'], 'data_request': [[]]})

        full_list = box.listMusic(args['music_folder'])
        for link in full_list:
            self.session['load_store'].append({'id': args['name_pointer'], 'data_request': [link[0]]})  # Need to wrap in [] because of some quirks
            logging.debug('list music %s', link)
            _temp = []
            audio_types = {'ogv': 'audio/ogg', 'ogg': 'audio/ogg', 'mp3': 'audio/mpeg'}
            for i, _l in enumerate(link[1]):
                _temp.append({'url': _l, 'type': audio_types[link[2][i]]})
            self.session['load_store'].append({'id': args['data_pointer'], 'data_request': [_temp]})

    def parseNewBrochures(self, args):
        requests.get(settings_local.backend_brochure_server_path+'new')


class fillDataOut(dataManagementBase):

    def __init__(self, config_fields, session):
        self.session = session
        self.config_fields = config_fields
        self.pg = session['pg_storage']
        self.formatter = Formatter()

    def saveDataStore(self, packet):
        field = filter(lambda field: field['id'] == packet['id'] and field['batch_id'] == packet['batch_id'], self.session['data_store'])
        #if field and packet['data_request'] != '':
        if field:
            field = field[0]
            field['data_request'] = packet['data_request']
            field['to_db'] = packet['data_request']
        print 'saveDataStore', filter(lambda field: field['id'] == packet['id'], self.session['data_store'])
        print packet

    def saveDataStore_option(self, packet):
        field_config = filter(lambda field: field['id'] == packet['id'], self.config_fields)[0]
        field_selected_id = field_config['data_args']['selected']

        field_selected = filter(lambda field: field['id'] == field_selected_id and field['batch_id'] == packet['batch_id'], self.session['data_store'])
        # In case there is only one field_selected with different batch_id. Needed for cases such as with invoice.
        if not field_selected:
            field_selected = filter(lambda field: field['id'] == field_selected_id, self.session['data_store'])
            if len(field_selected) != 1:
                field_selected = None

        field = filter(lambda field: field['id'] == packet['id'] and field['batch_id'] == packet['batch_id'], self.session['data_store'])
        #Get data and display directly from the field
        print 'SaveDS_opt', packet
        print filter(lambda field: field['id'] == packet['id'], self.session['data_store'])

        if field:
            field, field_data, field_display = field[0], field[0]['data_request']['data'], field[0]['data_request']['options']
            field['data_request']['selected'] = packet['data_request']['selected']
            index_data = field_display.index(packet['data_request']['selected'])

            #Save the display option to selected field to_db, but put the attached data into the original field's to_db
            field['to_db'] = field_data[index_data]
            logging.debug('saveds_option field %s', field)
            if field_selected:
                field_selected = field_selected[0]
                field_selected['data_request'] = packet['data_request']['selected']
                field_selected['to_db'] = packet['data_request']['selected']
                print field_selected['to_db']
        logging.debug('saveds_opt selected %s\t%s', field_selected_id, filter(lambda field: field['id'] == field_selected_id and field['batch_id'] == packet['batch_id'], self.session['data_store']))

    def pickle(self, required_fields, args):
        pickle = cPickle.dumps(required_fields)
        pickle = psycopg2.extensions.adapt(pickle)
        data = dict()
        data['pickle'] = pickle
        data = [data]
        self.pg.setDispatch(args['query_set_id'], data)
        return True

    def updateDB(self, packet, args):
        print 'updateDB', packet, args
        if 'view_id' in args:
            if self.session['endpoints']['form'].view_config['id'] != args['view_id']:
                return
        data = self.datastoreToQuery(fields=args['fields'], batch_id=packet['batch_id'])
        self.pg.setDispatch(args['query_set_id'], data)

    def saveDB(self, required_fields, args):
        data = self.datastoreToQuery()

        result = self.pg.setDispatch(args['query_set_id'], data)
        if result:
            local_store = {}
            for _result in result:
                keys = _result.keys()
                #for key in keys:
        #        #print '_result:', _result
                for key, value in _result.iteritems():
                    if not key in local_store:
                        local_store[key] = []
                    local_store[key].append(value)
            for key in local_store.keys():
                self.session['load_store'].append({'id': key, 'data_request': local_store[key]})
        return True

    def requestDBlink(self, required_fields, args):
        return self.requestDB(args)

    def dbFillOne(self, packet, args):  # This is for fetching a piece of data out of db. Mainly so that the program has a pointer to correct record in memory.
        data = self.datastoreToQuery()

        result = self.pg.setDispatch(args['query_set_id'], data)
        if result:
            result = result[0]
            for key, value in result.iteritems():
                fill_field = args['fill'][key]
                field = filter(lambda field: field['id'] == fill_field and field['batch_id'] == packet['batch_id'], self.session['data_store'])[0]
                field['to_db'] = value
                field['data_request'] = value
                if not value:
                    field['data_request'] = True

    def clearFields(self, packet, args):
        batch_id = packet['batch_id']
        for field_id in args['fields']:
            field = filter(lambda field: field['id'] == field_id and batch_id == field['batch_id'], self.session['data_store'])
            if field:
                field[0]['to_db'] = ''
                if args['hard']:
                    field[0]['data_request'] = False

    def removeFields(self, packet, args):
        fields = args['fields']
        print 'removeFields:', fields
        for field in fields:
            data_store = filter(lambda _field: field == _field['id'], self.session['data_store'])
            for rm in data_store:
                self.session['data_store'].remove(rm)
        self.session['load_store'] = []

    def transferCollapsed(self, required_fields, args):
        #Takes all required_fields and puts their data_requests in one array
        #Converts it into a set to de-duplicate and back into list for formatting
        skip = []  # This is so that when items from one field is de-duplucated, the items in the relevant list are removed accordingly
        for i, id in enumerate(args['from']):
            f_to = filter(lambda field: field['id'] == args['to'][i], self.session['data_store'])[0]
            f_from = filter(lambda field: field['id'] == id, required_fields)
            f_from = sorted(f_from, key=lambda row: row['batch_id'])
            #print 'to:', args['to'][i], 'form:', id
            #print 'transferCollapsed', f_from
            #Need to filter though required_fields

            self.session['log'].logStore(required_fields)
            self.session['log'].logStore(f_to, 'transferCollapsed')
            f_to['data_request'] = []
            for _f in f_from:
                f_to['data_request'].append(_f['data_request'])

            #Not knowing if int(item) is important. Will just do a isinstance check
            #Gives error upon floats
            nunique = False
            if 'nunique' in args:
                if id in args['nunique']:
                    nunique = True
            unique = set()
            def iTer():
                for z, item in enumerate(f_to['data_request']):
                    if z in skip: continue
                    #Items have to be unique, because of how templates view has been set up
                    #Nunique allows to prevent certain fields from being checked
                    if not nunique:
                        if item in unique:
                            skip.append(z)
                            continue
                        else:
                            unique.add(item)

                    try:
                        item = ast.literal_eval(item)
                    except ValueError:
                        pass
                    # Syntax Error happens on existence of space in string, therefore passing will just keep the object as a string object
                    except SyntaxError:
                        pass
                    yield item

            #f_to['data_request'] = [int(item) for item in f_to['data_request']]
            f_to['data_request'] = [x for x in iTer()]
            #f_to['data_request'] = list(set(f_to['data_request']))
            #print 'transferCollapsed', f_to['data_request']
            #print filter(lambda field: field['id'] == args['to'], self.session['data_store'])[0]
            #Set protects from duplicates

    def execParent(self, required_fields, args):
        #Args is just the parent view_id
        #Exec child is the flag. Alias for exec_parent*
        form = self.session['endpoints']['form']
        if form.exec_child:
            print 'execParent, true'
            current_id = copy.copy(form.view_config['id'])
            #remove_child = form.view_config['id']
            form.formSwitch(view_id=args)
            print 'execParent'
            try: form.skip_children.append(form.view_config['child'].index(current_id))
            except ValueError: pass
            form.on_submit()
            form.formSwitch(view_id=current_id)
            print 'execParent -- 2'

    def selectPreffered(self, required_fields, args):
        # Args - query_set_id,      replace
        # Queries. On data returned, finds the minimum number. Replaces (what) with found item
        # Query must return only one column.

        data = self.datastoreToQuery(args['query_set_id'])
        result = self.pg.setDispatch(args['query_set_id'], data)
        values = [_result.values() for _result in result]
        values = [value[0] for value in values]
        try:
            new = min(values)
        except:
            return

        replace = filter(lambda _f: _f['id'] == args['replace'], self.session['data_store'])[0]
        replace['data_request'] = new

        print 'selectPreffered', values, new

    def quickSearch(self, packet, args):
        self.requestDB(args)

    def _switchView(self, required_fields, args):
        self.session['controllers']['page'].switchView(args['view_id'])
        return True

    def default(self, required_fields, args):
        #Forces default on empty
        for store in [required_fields, self.session['data_store']]:
            fields = filter(lambda field: field['id'] in args['id'], store)  # Required fields instead of data_store. Transfer collapsed uses required_fields
            for field in fields:
                if field['data_request'] == '':
                    field['data_request'] = args['value']

    def generateBlog(self, required_fields, args):
        #This is a special case function. Queries are all hardcoded.
        #Limited to jpg
        #Rather dirty code by the way, but it'll work fine
        try:
            box = self.session['controllers']['box']
            order_id = filter(lambda field: field['id'] == args['order_id'], self.session['data_store'])[0]['data_request']
            folder_name = filter(lambda field: field['id'] == args['slug'], self.session['data_store'])[0]['data_request'].strip()
            blog_title = filter(lambda field: field['id'] == args['title'], self.session['data_store'])[0]['data_request'].strip()
            description = filter(lambda field: field['id'] == args['description'], self.session['data_store'])[0]['data_request'].strip()
            music_links = filter(lambda field: field['id'] == args['music'], self.session['data_store'])[0]
            #Music could either be in to_db or if its not changed from last time, it will be in data_request.to_db, or could be absent altogether
            if 'to_db' in music_links:
                music_links = music_links['to_db']
            elif 'to_db' in music_links['data_request']:
                music_links = music_links['data_request']['to_db']
            else:
                music_links = []
            logging.debug('generateBlog music_links %s', music_links)

            address = filter(lambda field: field['id'] == args['address'], self.session['data_store'])[0]['data_request'].strip()
            env = Environment(loader=FileSystemLoader('templates'))
            page_config = {"photos": {"name": "Photos", "url": "index.html"}, "panorama": {"name": "Virtual Tour", "url": "panorama.html"}, "map": {"name": "Area Map", "url": "map.html"},
                           "streetview": {"name": "Street View", "url": "streetview.html"}}
            blog_path = None

            order = []
            images = []
            _settings = {}

            #Built-in functions
            #loadOrder loads the list from a csv file, NOT DB
            #Dirty!
            def loadOrder():
                order_file = open('order.csv')
                _order = order_file.readlines()
                order_file.close()

                for line in _order:
                    parts = line.split(',')
                    append = {parts[0].strip(): parts[1].strip()}
                    order.append(append)
            def sortImages(images):
                image_list = []
                pano_list = []
                image_list_ = []
                pano_list_ = []

                for image in images:
                    image_path_split = os.path.splitext(image['name'])
                    image['name'] = image_path_split[0]
                    image['name'] = re.sub('[0-9]*$', '', image['name'])
                    pano_match = re.match('pano([a-zA-Z]*)', image['name'])
                    if pano_match:
                        pano_list.append({'name': pano_match.group(1), 'url': image['url']})
                    else:
                        image_list.append({'name': image['name'], 'url': image['url'], 'box_id': image['box_id']})

                for row in order:
                    for image in image_list:
                        if image['name'] in row:
                            image_list_.append({'name': row[image['name']], 'url': image['url'], 'box_id': image['box_id']})
                    for pano in pano_list:
                        if pano['name'] in row:
                            pano_list_.append({'name': row[pano['name']], 'url': pano['url']})

                return_data = {'panos': pano_list_, 'images': image_list_}
                return return_data
            def makeIndex(images, music, settings):
                template = env.get_template('index.html')
                page_present = filter(lambda page_data: page_data['name'] == 'Photos', settings['pages'])
                if page_present:
                    page_present[0]['class'] = 'mlink_span_c'

                rendered = template.render(images=images, music=music, settings=settings)

                index_file = blog_path + 'index.html'
                open_index = open(index_file, 'w')
                open_index.write(rendered)
                open_index.close()

                if page_present:
                    page_present[0]['class'] = ''
            def makePano(images, settings):
                if images:
                    page_present = filter(lambda page_data: page_data['name'] == 'Virtual Tour', settings['pages'])
                    if page_present:
                        page_present[0]['class'] = 'mlink_span_c'

                    template = env.get_template('panorama.html')

                    for i, image in enumerate(images):
                        image['count'] = i

                    rendered = template.render(images=images, settings=settings)

                    pano_file = blog_path + 'panorama.html'
                    open_index = open(pano_file, 'w')
                    open_index.write(rendered)
                    open_index.close()

                    if page_present:
                        page_present[0]['class'] = ''
            def makeMap(settings):
                page_present = filter(lambda page_data: page_data['name'] == 'Area Map', settings['pages'])
                if page_present:
                    page_present[0]['class'] = 'mlink_span_c'
                template = env.get_template('map.html')
                rendered = template.render(settings=settings)

                map_file = blog_path + 'map.html'
                open_index = open(map_file, 'w')
                open_index.write(rendered)
                open_index.close()

                if page_present:
                    page_present[0]['class'] = ''
            def makeStreetView(settings):
                page_present = filter(lambda page_data: page_data['name'] == 'Street View', settings['pages'])
                if page_present:
                    page_present[0]['class'] = 'mlink_span_c'

                template = env.get_template('streetview.html')
                rendered = template.render(settings=settings)

                map_file = blog_path + 'streetview.html'
                open_index = open(map_file, 'w')
                open_index.write(rendered)
                open_index.close()

                if page_present:
                    page_present[0]['class'] = ''

            #Setting which pages will be displayed
            #In config the sequence must match below
            bool_photos = filter(lambda field: field['id'] == args['bools'][0], self.session['data_store'])[0]['data_request']
            bool_pano = filter(lambda field: field['id'] == args['bools'][1], self.session['data_store'])[0]['data_request']
            bool_map = filter(lambda field: field['id'] == args['bools'][2], self.session['data_store'])[0]['data_request']
            bool_streetview = filter(lambda field: field['id'] == args['bools'][3], self.session['data_store'])[0]['data_request']
            bool_music = filter(lambda field: field['id'] == args['bool_music'], self.session['data_store'])[0]['data_request']
            _p1 = ['photos', 'panorama', 'streetview', 'map']
            _p2 = [bool_photos, bool_pano, bool_map, bool_streetview]
            pages_present = zip(_p1, _p2)
            _settings['pages'] = []
            for page in pages_present:
                if page[1] == True:
                    _settings['pages'].append(page_config[page[0]])

            _settings['title'] = blog_title
            _settings['description'] = description
            _settings['address'] = address
            _settings['bool_music'] = bool_music
            #If NULL then do not have anything for the title or description
            if _settings['title'] == 'NULL': _settings['title'] = ''
            if _settings['description'] == 'NULL': _settings['description'] = ''
            #Finding folder and saving all the images on the hard drive. Then appending them to an array and creating pages.
            folder_id = box.execRequest(box.findFolder, {'folder_name': folder_name, 'parent': settings.box_data['retoucher_folder_id']})
            if folder_id:
                folder_id = folder_id['folder_id']
                #Creating appropriate folders. For the sake of simplicity, removing if already exists
                blog_path = args['store_path'] + folder_name + '/'
                photos_path = blog_path + 'photos/'
                if not os.path.exists(blog_path):
                    os.makedirs(blog_path)
                    os.makedirs(photos_path)
                else:
                    shutil.rmtree(blog_path)
                    os.makedirs(photos_path)

                #This is to make sure that the token is fresh right before start of downloading process
                box.refreshToken()
                for file_data in box.downloadFolder(folder_id):
                    #Temporary logging solution
                    self.session['endpoints']['form'].emit('error', str(file_data['file_id']))
                    ###

                    file_name = photos_path + file_data['file_id'] + '.jpg'
                    photo = open(file_name, 'w+')
                    try:
                        file_data['content'] = box.reduceSize(file_data['content'])
                        photo.write(file_data['content'])
                        images.append({'name': file_data['name'], 'url': 'photos/'+file_data['file_id']+'.jpg', 'box_id': file_data['file_id']})
                    except BaseException as e:
                        self.session['endpoints']['form'].emit('error', 'Could not save file. Please try again. Original error: ' + str(e))
                    finally:
                        photo.close()

                loadOrder()  # Order of files
                sorted_images = sortImages(images)

                #Not very good because using a set index 1 (set string 'panorama') but good enough for now.
                if not sorted_images['panos']: del _settings['pages'][1]
                makeIndex(images=sorted_images['images'], music=music_links, settings=_settings)
                makePano(images=sorted_images['panos'], settings=_settings)
                makeMap(_settings)
                makeStreetView(_settings)

                #Here goes the function of saving sorted_images['images'] to db.
                query = "INSERT INTO image_links (order_id, name, box_id, server_link) VALUES (%s, %s, %s, %s)"
                query_clear = "DELETE FROM image_links WHERE order_id = %s"

                self.pg.cursorReload()
                self.pg.cursor.execute(query_clear, [order_id])
                for image in sorted_images['images']:
                    #name, url, box_id
                    self.pg.cursor.execute(query, [order_id, image['name'], image['box_id'], folder_name+'/'+image['url']])
                self.pg.con.commit()
            else:
                self.session['endpoints']['form'].emit('error', 'Could not find folder in the box directory')
                #return False  #Disabaled temporarily
        except BaseException as e:
            self.session['endpoints']['form'].emit('error', 'Generate blog failed: ' + str(e))
            return False

    def newOrderBrochure(self, unknown, args):
        field = filter(lambda field: field['id'] == args['order_id'], self.session['data_store'])

        end_path = 'new_order/{0}'.format(field[0]['data_request'])
        requests.get(settings_local.backend_brochure_server_path+end_path)
