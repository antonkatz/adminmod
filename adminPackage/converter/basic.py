from gevent import monkey; monkey.patch_all()

def reduceAttrs(array, final_fields): #both must be lists. final_fields contains the names of dict elements in array list
    return_list = []
    temp_list = {}
    for row in array:
        for field in final_fields:
            temp_list[field] = row[field]
        return_list.append(temp_list)
        temp_list = {}
    return return_list

def splitBatches(fields):
    batches = []
    i = min(fields, key=lambda field: field['batch_id'])['batch_id']
    max_batch_id = max(fields, key=lambda field: field['batch_id'])['batch_id']

    while i <= max_batch_id:
        batches.append([field for field in fields if field['batch_id'] == i])
        i += 1

    return batches