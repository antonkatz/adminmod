from gevent import monkey; monkey.patch_all()
import re


class formCheck:
    def __init__(self, form_obj, session):
        self.session = session
        self.form = session['endpoints']['form']

    def reqCheckField(self, field, batch_id):
        data_store = self.session['data_store']
        update_req = field['update_req']
        print 'reqCheckField', field, self.form.view_config['id'], self.form.fields
        #Field already seems to possess update_req
        #update_req = filter(lambda fields_row: fields_row['id'] == field['id'], self.form.fields)[0]['update_req']

        for req in update_req:
            if req:
                check_field = filter(lambda fields_row: fields_row['id'] == req and batch_id == fields_row['batch_id'], data_store)[0]
                if not check_field['data_request']:
                    return {'result': False, 'missing': req}
        return {'result': True}

    def reCheckField(self, packet):
        print 'reCheck', packet, self.form.view_config['id']
        id = packet['id']
        field_config = filter(lambda field: field['id'] == id, self.form.fields)[0]
        if 're' in field_config:
            regexp = field_config['re']
            data_request = packet['data_request']
            ###WHAT ABOUT THE FIELDS WITH OPTIONS
            if data_request == '':
                return True
            match = re.match(regexp, data_request)
            if match:
                return True
            return False
        return True

    def reqCheckForm(self, requirements):

        ###
        # Going to have to redo this part. Some requirements are batch dependant, but some are not.
        # In case there are several fields, do batch check, otherwise single check?
        ###

        data_store = self.session['data_store']
        print 'reqCheckForm', data_store
        #Since many forms can be stored at once, batch ids sometimes are larger then within a max in a certain form
        data_store = filter(lambda field: field['id'] in requirements, data_store)
        batches = []

        self.session['log'].logStore(data_store)
        first_req = filter(lambda field: field['id'] == requirements[0], data_store)
        if not first_req:
            return False

        max_batch_id = max(first_req, key=lambda field: field['batch_id'])['batch_id']
        min_batch_id = min(first_req, key=lambda field: field['batch_id'])['batch_id']

        self.session['log'].logStore(range(max_batch_id+1))
        for i in range(min_batch_id, (max_batch_id+1)):
            batch = [field for field in data_store if field['batch_id'] == i]
            #Sometimes there are gaps. So append only if something in the list
            if batch:
                batches.append(batch)
        #self.session['log'].logStore(first_req, 'ds reqCheckForm')
        print min_batch_id, max_batch_id, range(min_batch_id, (max_batch_id+1))
        print batches

        flag_pass = True
        #print 'dt:', self.session['data_store']
        #print batches
        self.session['log'].logStore('checking...')
        for batch in batches:
            for req in requirements:
                #print '---\t reqCheckForm', batch, req
                self.session['log'].logStore((batch, req))

                try:  # A very temporary solution. Destroys the effectiveness of the check.
                    field = filter(lambda row: row['id'] == req, batch)[0]
                    self.session['log'].logStore(field)
                    if not field['data_request'] and 'extra' in field:
                        if not field['extra']:
                            flag_pass = False
                            print 'failed at', field
                            break
                except IndexError:
                    pass
            if not flag_pass:
                break

        self.session['log'].logStore(flag_pass)
        self.session['log'].log('reqCheckForm')
        return flag_pass