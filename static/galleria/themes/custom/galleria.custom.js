(function($) {

/*global jQuery, Galleria */

Galleria.addTheme({
    name: 'custom',
    author: 'Galleria',
    css: 'galleria.custom.css',
    defaults: {
        transition: 'fade',
        thumbCrop:  'height',

        // set this to false if you want to show the caption all the time:
        _toggleInfo: false
    },
    init: function(options) {

        Galleria.requires(1.28, 'This version of Classic theme requires Galleria 1.2.8 or later');

        // add some elements
        //this.addElement('info-link','info-close');
        //this.append({
        //    'info' : ['info-link','info-close']
        //});
        
        this.addElement('menu', 'show-thumb', 'play', 'full', 'next', 'prev', 'lightbox', 'soundicon');
        var menu = this.$('menu');
        var show_thumb = $('<a id="showThumbTooltip" title="Thumbnails">');
        this.$('show-thumb').appendTo(show_thumb);
        
        var play_b = $('<a id="playTooltip" title="Play/Pause">');
        this.$('play').appendTo(play_b);
        var full_b = $('<a id="fullTooltip" title="Fullscreen">');
        this.$('full').appendTo(full_b);
        var next_b = this.$('next');
        var prev_b = this.$('prev');
        var lightbox_b = $('<a id="lightboxTooltip" title="Lightbox">');
        this.$('lightbox').appendTo(lightbox_b);
        var sound_b = $('<a id="soundiconTooltip" title="Music">');
        sound_b.css('display', 'none');
        this.$('soundicon').appendTo(sound_b);
        this.$('soundicon').addClass('mon');
        menu.append(show_thumb, play_b, full_b, lightbox_b, sound_b);
        this.$('container').append(menu);
        this.$('info').appendTo(menu);
        
                var toggle_sound = 1;
                function toggleSound() {
                    if (toggle_sound == 0){
                        document.getElementById('mplayer').play();
                        toggle_sound = 1;
                        gallery.$('soundicon').addClass('mon');
                    }else{
                        toggle_sound = 0;
                        document.getElementById('mplayer').pause();
                        gallery.$('soundicon').removeClass('mon');
                    }
                }
        sound_b.click(toggleSound);
        
        var thumb_cont = this.$('thumbnails-container');
        show_thumb.click(function(){
            thumb_cont.toggle();
            });
        
        thumb_cont.append(next_b);
        thumb_cont.append(prev_b);
        
        var gallery = this;
        this.setPlaytime(3000);
        play_b.click(function(){
            gallery.playToggle();
            gallery.$('play').toggleClass('galleria-pause');
        });
        
        full_b.click(function(){gallery.toggleFullscreen()});
        lightbox_b.click(function(){gallery.openLightbox();});
        
        var thumbnails_c = this.$('thumbnails');
        thumbnails_c.addClass('flexcroll');
        //console.log($(thumbnails_c).height());
        
        next_b.click(function(){
            var next_thumb = thumbnails_c.scrollTop() + 467;
            current_top_thumb = next_thumb;
           thumbnails_c.scrollTop(next_thumb);      
            });
        prev_b.click(function(){
            var next_thumb = thumbnails_c.scrollTop() - 467;
            current_top_thumb = next_thumb;
            thumbnails_c.scrollTop(next_thumb);
            });

        // cache some stuff
        var info = this.$('info-link,info-close,info-text'),
            touch = Galleria.TOUCH,
            click = touch ? 'touchstart' : 'click';

        // show loader & counter with opacity
        this.$('loader,counter').show().css('opacity', 0.4);

        // some stuff for non-touch browsers
        if (! touch ) {
            this.addIdleState( this.get('image-nav-left'), { left:-50 });
            this.addIdleState( this.get('image-nav-right'), { right:-50 });
            this.addIdleState( this.get('counter'), { opacity:0 });
        }

        // toggle info
        if ( options._toggleInfo === true ) {
            info.bind( click, function() {
                info.toggle();
            });
        } else {
            info.show();
            this.$('info-link, info-close').hide();
        }

        function toggleThumb(){
            gallery.$('loader').show();
            setTimeout(function(){
                thumb_cont.hide();
                gallery.$('loader').hide();
                       },1000);
        }
        
        // bind some stuff
        this.bind('thumbnail', function(e) {
            
            var caption = $('<div>');
            caption.addClass('galleria-thumb-text');
            caption.append(e.galleriaData.title);
            //console.log(e.galleriaData);
            var thumb_div = $(e.thumbTarget).parent();
            $(thumb_div).append(caption);
            
            if (! touch ) {
                // fade thumbnails
                $(e.thumbTarget).click(function(){
                    //thumb_cont.toggle();
                    toggleThumb();
                    });
                $(e.thumbTarget).css('opacity', 0.6).parent().hover(function() {
                    $($(this).not('.active').children()[0]).stop().fadeTo(100, 1);
                }, function() {
                    $($(this).not('.active').children()[0]).stop().fadeTo(400, 0.6);
                });

                if ( e.index === this.getIndex() ) {
                    $(e.thumbTarget).css('opacity',1);
                }
            } else {
                $(e.thumbTarget).css('opacity', this.getIndex() ? 1 : 0.6);
            }
        });

        this.bind('loadstart', function(e) {
            if (!e.cached) {
                this.$('loader').show().fadeTo(200, 0.6);
            }
            console.log($(thumbnails_c).scrollTop());
            this.$('info').toggle( this.hasInfo() );

            $(e.thumbTarget).css('opacity',1).parent().siblings().children().css('opacity', 0.6);
        });

        //$(window).load(function(){
        this.bind('loadfinish', function(e) {
        //console.log('eh');
            this.$('loader').hide();
        });
        
                $('#showThumbTooltip').aToolTip();
                $('#lightboxTooltip').aToolTip();
                $('#fullTooltip').aToolTip();
                $('#playTooltip').aToolTip();
                
                
                if(window.global_audio == 1){
                    sound_b.css('display', 'block');
                    $('#soundiconTooltip').aToolTip();
                }
                
        //$(document).load(function(){
        //    console.log('eh');
        //});
        
    }
});

}(jQuery));