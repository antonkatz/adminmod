from gevent import monkey; monkey.patch_all()

from socketio.server import SocketIOServer
from adminPackage import Application
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    print 'Listening on port http://0.0.0.0:5000 and on port 10843 (flash policy server)'
    server = SocketIOServer(('0.0.0.0', 5000), Application(),
        resource="socket.io", policy_server=True,
        policy_listener=('0.0.0.0', 10843)).serve_forever()